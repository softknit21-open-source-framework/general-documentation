# -*- coding: utf-8 -*-
""" This file is part of SoftKnit21 framework

License
=======

 - B{SofKnit21} (U{http://www.softkni21.org}) is Copyright:
  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the Affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

common helpers

Implements
==========

 - B{helpers}

Documentation
=============

    helpers functions common to all modules

Usage
=====

@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


from gettext import gettext as _
import enum
from SoftKnit21.commons.SoftKnit21Errors import BadImplementationError



# def key_for_value(value, dic):
    # for k in dic.keys():
        # if dic[k] == value:
            # return k
    # else:
        # raise SoftKnit21Error(
            # _(f" no key found for {value} in {dic}"))


def _check_string_binary(string):
    import re
    if re.search('[^0^1]', string) == None:
        return True
    else:
        return False


def contains(small, big):
    """ test inclusion of small list in big list
    returns False is list not included
    returns indices of the big list corresponding to the small list
    @param small : small list
    @type : list

    @param small : big list
    @type : list
    """
    for i in range(len(big) - len(small) + 1):
        for j in range(len(small)):
            if big[i + j] != small[j]:
                break
        else:
        # else of the for close not of the if clause
        # runs at the end of the for loop if no break occured
                return i, i + len(small)
    return False


def is_gen_empty(iterable):
    """ returns True if iterable is empty false otherwise"""
    try:
        first = next(iterable)
    except StopIteration:
        return True
    return False

class HEnum(str, enum.Enum):
    """an Enum class to define enumerations containing 3 attributes for each member: value, label and help"""

    def __new__(cls, value, label, help):
        obj = str.__new__(cls, value)
        obj._value_ = value
        obj.label = label
        obj.help = help
        return obj

    @classmethod
    def check_compatible(cls, other_enum, optional=None):
        """check that other_enum is compatible with self:
        compatible means all members of self are in other_enum except members listed in optional
        each member of other_enum has same code as self"""
        if optional:
                if not isinstance(optional, list):
                    optional = [optional]
                optional_keys = [op.name for op in optional]
        else:
            optional_keys = []
        for key, member in cls.__members__.items():
            m1 = f' {key} member of {cls.__qualname__} cannot be omited'

            try:
                assert ((key in other_enum.__members__.keys()) or (optional and key in optional_keys)), m1
                if not key in optional_keys:
                    m2 = f'{cls.__qualname__} member {key} cannot be overriden by {getattr(other_enum, key)}'
                    othermember = getattr(other_enum, key)
                    assert member.value == othermember.value, m2
                    assert member.name == othermember.name, m2
            except AssertionError as e:
                raise BadImplementationError(e.args) from e

class classproperty:
    """
    Decorator that converts a method with a single cls argument into a property
    that can be accessed directly from the class.
    taken from django/utils/functional
    """

    def __init__(self, method=None):
        self.fget = method

    def __get__(self, instance, cls=None):
        return self.fget(cls)

    def getter(self, method):
        self.fget = method
        return self    

if __name__ == "__main__":

    import unittest
    import time

    unittest.main()
