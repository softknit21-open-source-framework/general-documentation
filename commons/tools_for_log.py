#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  Copyright 2019 odile.troulet-lambert.tex@orange.fr
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License 3 as published by
#  the Free Software Foundation;
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.


"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Tools for logging

Implements
==========



Documentation
=============



Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""


import logging
# from logging.handlers import TimedRotatingFileHandler
import logging.config
import json
mlog = logging.getLogger(__name__)


class StrucLogMessage:
    """ structured message encoded in json to be logged
    this allows log messages and in particular dictionnaries to be worked on by programm
    as compared to dictionnaries being logged into a string format
    presently used in tests in conjunction with mocking logging function
    using json.loads(str(<one call arguments>[1]))

    @att kwargs : names and values of the data in the message """

    def __init__(self, mess='', **kwargs):
        self.kwargs = kwargs
        if mess != '':
            self.kwargs.update({'message': mess})

    def __str__(self):
        # the log uses python 2 string formatting for backward compatibility
        return '%s' % (json.dumps(self.kwargs))


class BraceMessage:
    """ to  use new f formatting in logs, because logs use old python formatting """

    def __init__(self, fmt, *args, **kwargs):
        self.fmt = fmt
        self.args = args
        self.kwargs = kwargs

    def __str__(self):
        return self.fmt.format(*self.args, **self.kwargs)


def get_TID():
    # to be replaced in 3.8 by get_native_id
    """Get TID as displayed by htop."""
    import ctypes
    libc = 'libc.so.6'
    # syscall() is a small library function that invokes the system call
    # whose assembly language interface has the specified number
    # with the specified arguments.
    try:
        for cmd in (186, 224, 178):
            tid = ctypes.CDLL(libc).syscall(cmd)
            if tid != -1:
                res = str(tid)
    except:
        res = None
    return res

# if it turns out necessary add the TID of the thread in the log record
# it ca be done using a filter :
# see here https://docs.python.org/3/howto/logging-cookbook.html#context-info
# it seems preferable to use filter than logger adapter because of the extensive use of logging mock in tests modules


def logconf(file, level=logging.DEBUG, append=None, test=False):
    """creates the logs and their tools;  to be called only by the main"""

    DEBUG_LOG_FILENAME = file + '.log'
    # create the logger and gives the level
    logger = logging.getLogger()
    logger.setLevel(level)

    if not logger.hasHandlers():
        loghandler = logging.FileHandler(
            DEBUG_LOG_FILENAME, mode='w', encoding='UTF-8')

        loghandler.setLevel(level)
        # add the formatter to the handler
        formatter = logging.Formatter('{asctime} {levelname} {module} {lineno} {threadName} {message}', style='{')
        loghandler.setFormatter(formatter)
        # add the handler to the logger
        logger.addHandler(loghandler)

    return logger


def search_in_log(call_args_list, string, lev=logging.DEBUG, key=None):
    """
    search in call args_list of the mocklog according to some criteria
    key = None : returns True if string is present in one of the simple log entries
    key != None returns True if sting is found in the entry key of the one of the StrucLogMessage
    string == '' returns True if no entry with level is found in the list
    """
    if not call_args_list or len(call_args_list) == 0:
        return False
    if string != '':
        for (level, arg), emptydic in call_args_list:
            if (level == lev and isinstance(arg, str)
                    and string in arg):
                return True
            if (level == lev and isinstance(arg, StrucLogMessage)):
                dic = json.loads(str(arg))
                if key == None:
                    mess = dic.get('message')
                    if mess is not None and string in mess:
                        return True
                elif key in dic.keys():
                    if isinstance(string, str) and string in dic.get(key):
                        return True
                    if not isinstance(string, str) and string == dic.get(key):
                        return True
                else:
                    return False
    elif string == '':
        for (level, arg), emptydic in call_args_list:
            if level == lev:
                return False
        return True

    return False


if __name__ == '__main__':
    import unittest
    from unittest.mock import patch

    mlog = logconf(__name__, logging.DEBUG)

    class Test_search_in_log (unittest.TestCase):

        def setUp(self):
            var = 1
            with patch('__main__.mlog', autospec=logging.Logger, wraps=logging.Logger) as mocklog:
                LCMessagea = {'mtype': 'a', 'class': 'normal', 'mesNumber': 20, 'checksum': True}
                LCMessageb = {'mtype': 'b', 'class': 'normal', 'mesNumber': 30, 'checksum': True}
                mlog.log(logging.DEBUG, f'message premier {var}')
                mlog.log(logging.CRITICAL, StrucLogMessage('message LogStrucLog critical message a', **LCMessagea))
                mlog.log(logging.INFO, StrucLogMessage('message LogStrucLog debug message b', **LCMessageb))
                mlog.log(logging.DEBUG, StrucLogMessage('', **LCMessageb))
                mlog.log(logging.INFO, ' info')

            self.call_list = mocklog.log.call_args_list

        def test_find_string_for_level_no_key(self):
            # in first arg message
            self.assertTrue(search_in_log(self.call_list, 'premier', logging.DEBUG,))
            # in arg message formated
            self.assertTrue(search_in_log(self.call_list, '1', logging.DEBUG,))
            # in not first arg message
            self.assertTrue(search_in_log(self.call_list, 'info', logging.INFO, ))
            # in struclogmessage
            self.assertTrue(search_in_log(self.call_list, 'debug', logging.INFO,))
            # in several messages
            self.assertTrue(search_in_log(self.call_list, 'LogStrucLog', logging.CRITICAL,))
            # wrong level
            self.assertFalse(search_in_log(self.call_list, 'info', logging.DEBUG,))
            # not present in any message
            self.assertFalse(search_in_log(self.call_list, 'critical', logging.DEBUG))

        def test_find_a_key(self):
            self.assertTrue(search_in_log(self.call_list, 'b', logging.INFO, key='mtype'))
            self.assertTrue(search_in_log(self.call_list, 'nor', logging.CRITICAL, key='class'))
            self.assertFalse(search_in_log(self.call_list, 'a', logging.INFO, key='mtype'))
            self.assertFalse(search_in_log(self.call_list, 'normal', logging.DEBUG, key='toto'))
            self.assertTrue(search_in_log(self.call_list, 30, logging.INFO, key='mesNumber'))

        def test_string_empty_do_not_find_level(self):
            self.assertTrue(search_in_log(self.call_list, '', logging.WARNING))
            self.assertFalse(search_in_log(self.call_list, '', logging.CRITICAL))

    unittest.main()
