# GUI

This project is about the Graphical User Interface of the SoftKnit21 open source framework

## Project status
software for GUI is not written yet. The GUI_global_specifications document gives the ideas about the user interface.
Using the pencil software 2 sketches are being designed :
- start which describes  the steps for the user to choose the swatch or the SWKPartModel to be knitted
- knitting which describes the user interface during the knitting process.

Each sketch can be opened : open the index.html file in the browser. The sketch is clickable in order to see the flow of the user interface.

Python software needs to run on the local computer connected to the lock controler. Functions of this softaware are :
- manages the lock_controler dialog,
- computes the data to be displaid by the user interface,
-  access to the local database containing previously knitted objects and notes that the user added for each knitted object.

Architecture choices for the user interface should be discussed.
The current idea is to use a client server architecture :
- the user interface is displaid by the local browser using javascript 
- the python software is included in a local python server to serve the html and javascript .

First thought is to use ReactJs components for javascript part and Klask for the server. Communications 
between client and local server would be done through websockets.

This architecture choices need to be discussed with whoever would be willing to be involved 
in the development of the user interface.

