# -*- coding: utf-8 -*-
""" This file is part of SoftKnit21 framework

License
=======

 - B{SofKnit21} (U{http://www.softkni21.org}) is Copyright:
  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the Affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program was started from the FSM from Py4bot from
Frederic Mantegazza see here :
http://py4bot.gbiloba.org is Copyright:
  - (C) 2014-2019 Frédéric Mantegazza


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============
implements the class FSM, used a a super class for all FSMs of the LCProtocol Manager


Implements
==========


Documentation
=============


Usage
=====

@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

import logging
from common.SoftKnit21Errors import FSMError
from common.tools_for_log import StrucLogMessage

mlog = logging.getLogger(__name__)


class FSM(object):
    """ Finite State Machine (FSM) object

    @ivar _initialState: initial state when launching the fsm
    @type _initialState: str

    @ivar _state: current state of the fsm
    @type _state: str
    """

    def _addTransition(self, event, src, dst, onCondition=None, onBefore=None, onLeave=None, onEnter=None, onReEnter=None, onAfter=None):
        """ Add a transition

        Add a transition from state 'src' to 'dst' for event name 'event'.
        if onCondition is not None, the transition is conditional, 
        depending on the condition True of False, the first callable or second callable 
        is chosen in the onBefore, onLeave, ... which are lists
        

        When the transition matches, the execution does:
         - onBefore(event)
         - onLeave(event)
         - onEnter(event)    # when dst != src
         - onReEnter(event)  # when dst == src
         - onAfter(event)

        If onBefore() returns 'cancel', the event is canceled.
        to avoid onBefore etc...to be executed in _addTransition use Action class

        @param event: name of the transition event
        @type event: src

        @param src: source state this transition apply to
        @type src: str or list/tuple of str

        @param dst: destination state this transition leads to
        @type dst: str

        @param onBefore: called before triggering the event
        @type onBefore: Action or callable

        @param onLeave: called before leaving the current state
        @type onLeave: Action or callable

        @param onEnter: called before reaching the destination state (when dst != src)
        @type onEnter: Action or callable

        @param onReEnter: called before re-entering the destination state (when dst == src)
        @type onReEnter: Action or callable

        @param onAfter: called after triggering the event
        @type onAfter: Action or callable
        """

        trans = self._transitions.get(event)
        if trans is None:
            self._transitions.update({event: dict([(src, (dst, onCondition, onBefore, onLeave,
                                                          onEnter, onReEnter, onAfter)), ])})
        else:
            # check that same event, same src does not exist
            if src in trans.keys():
                raise FSMError(f'transition event : {event}, src : {src} already exits')
            self._transitions[event].update({src: (dst, onCondition, onBefore, onLeave,
                                                   onEnter, onReEnter, onAfter)})
        # todo check that for a conditional transition, the onBefore .... are lists
        return

    def _choose(self, tupl, condition):
        """ returns an updated tuple with values corresponding to the condition
        @param tupl : tuple of the transition
        @type : tuple

        @param condition : condition
        @type : boolean"""
        li = []
        for t in tupl:
            if isinstance(t, tuple):
                li.append(t[1 * condition])
            else:
                li.append(t)
        return tuple(li)

    def trigger(self, evName, **kwargs):
        """ Trigger the given event

        @param evName: name of the event to process
        @type name: str

        @param kwargs : data to be used by the transition callbacks
        @type : dictionnary
        """

        if evName is None:
            raise FSMError(
                f'event dictionnary does not contain an event name')
        mlog.log(logging.DEBUG, f'FSM.trigger {evName} in state {self._state}')
        mlog.log(logging.DEBUG, StrucLogMessage(**kwargs))

        src = self._state
        if src == None:

            raise FSMError(
                _(f'{self.name} has no state defined'))

        # Get all transitions associated with this event or * event in an ordered dict
        import collections
        eventTransitions = collections.OrderedDict()
        eventTransitions[evName] = self._transitions.get(evName)
        eventTransitions['*'] = self._transitions.get('*')
        forThisSrc = None
        for event, transitions in eventTransitions.items():
            if transitions is not None:
                forThisSrc = [tupl for source, tupl in transitions.items() if source == src]
                if len(forThisSrc) == 0:
                    forThisSrc = [tupl for source, tupl in transitions.items() if source == '*']
                if len(forThisSrc) != 0:
                    break
        if forThisSrc is None or len(forThisSrc) == 0:
            mlog.log(logging.CRITICAL, f'ignoring unknown or undefined event {evName} for current state {src}')
            return
        else:
            forThisSrc = forThisSrc[0]
        # Build eventData dict
        eventData = {'evName': evName, 'src': src}
        eventData.update(kwargs)
        dst, onCondition, onBefore, onLeave, onEnter, onReEnter, onAfter = forThisSrc
        if onCondition is not None:
            cond = onCondition[0](*onCondition[1:], **eventData) if isinstance(onCondition, list) else onCondition(**eventData)
            forThisSrc = self._choose(forThisSrc, cond)
            dst, onCondition, onBefore, onLeave, onEnter, onReEnter, onAfter = forThisSrc
            eventData.update({'cond': cond})

        if onBefore is not None:
            result = onBefore[0](*onBefore[1:], **eventData) if isinstance(onBefore, list) else onBefore(**eventData)

            if result == 'cancel':
                mlog.log(logging.DEBUG, f'canceling event {evName} current state {src}')
                return
        if onLeave:
            onLeave[0](*onLeave[1:], **eventData) if isinstance(onLeave, list) else onLeave(**eventData)

        if dst == '=' or dst == src:
            mlog.log(logging.DEBUG, f'on event : {evName} remaining in current state {src}')
            if onReEnter:
                onReEnter[0](*onReEnter[1:], **eventData) if isinstance(onReEnter, list) else onReEnter(**eventData)
        else:
            self._state = dst
            mlog.log(logging.DEBUG, f'on event : {evName} moving into  in state {dst}')
            if onEnter:
                onEnter[0](*onEnter[1:], **eventData) if isinstance(onEnter, list) else onEnter(**eventData)
        if 'on_msg_in' in evName:
            # this is a message received event, log the data
            mlog.log(logging.DEBUG, StrucLogMessage(**eventData))
        if onAfter:
            onAfter[0](*onAfter[1:], **eventData) if isinstance(onAfter, list) else onAfter(**eventData)
        return

    def _createTransitions(self):
        """ Create the transitions if they are not created by constructor

        Must be overloaded in sub-classes
        """
        raise NotImplementedError

    def __init__(self, initialName='idle', name='main', triggerFirst=None, parent=None, transitions=None, createTransitions=False):
        """ constructor of the FSM creates the transitions

        @param initialName : name of the initial state
        @type : string

        @param name : name of the fsm
        @type : string

        @param triggerFirst : event for the first transtion to be triggered by the fsm constructor
        @type : string

        @param transitions : transitions for the fsm if transitions are data
        @type : list of dict or None
        """

        self._state = initialName
        self.parent = parent
        self._initialState = initialName
        self.name = name
        self._transitions = {}
        if createTransitions:
            if transitions is not None:
                # create transitions from a dictionnary
                for transition in transitions:
                    self._addTransition(**transition)
            else:
                # create Transitions with the method
                self._createTransitions()
            if triggerFirst is not None and triggerFirst in self._transitions.keys():
                self.trigger(triggerFirst)

    def __str__(self):
        return self.name

    @property
    def state(self):
        return self._state

    def start(self):
        """ Reset FSM """
        self._state = self._initialState

    def set_state(self, state):
        self._state = state


if __name__ == "__main__":

    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__name__', test=True)

    from pathlib import Path
    current = Path(__file__)
    tests_dir = Path(current.parent, 'tests')
    loader = unittest.TestLoader()
    tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)
