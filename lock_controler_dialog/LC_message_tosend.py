#!/usr/bin/env python
# coding:utf-8
"""
  This file is part of SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============
class and generic functions to send a message to the LC

Implements
==========

LCMessagetoSend

Documentation
=============

    
Usage
=====
exceptions raised only for errors from programming environement
errors which might come from transmission on serial port are not raised but logged at critical level

@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


from common. SoftKnit21Errors import LCMessageError
from lock_controler_dialog.LC_Helpers import LCProtocolHelpers
from lock_controler_dialog.LC_message_abstract import LCMessage, Payload
import logging

mlog = logging.Logger(__name__)


class LCMessagetoSend (LCMessage):
    """ contains a LCMessage subclass of LCMessage abstract class

    @mtype : type of the message
    @type : ascii character

    @lenpayload : length of the payload of the message
    @type : int (-1 for a single byte message)

    @payload : payload of the message
    @type : list of integers

    @checksum : value of the checksum
    @type : integer
    
    @timer : reference of the timer set while sending(attribute handled by the LcokControler)
    @type : threading.timer
    
    @repeat : number of retries to send the message (attribute handled byt the LockControler)
    @type : integer

    """

    def set_checksum(self):
        """ sets the ceck sum of the message """
        if self.mtype.isupper():
            # a singlebyte message has no checksum
            self._checksum = None
            return
        if self.payload == []:
            self._checksum = None
            return
        check = ord(self.mtype) + self.lenpayload + sum(self.payload)
        self._checksum = (check % 128)
        return

    def __init__(self, **kwargs):
        """ create an instance of a LCMessagetoSend from kwargs"

        @param mtype : type of the message
        @type : ascii character

        @param kwargs : dictionnary of all values for the message
        @type : dictionnary"""

        self.datadict = {}
        self._checksum = None
        self.bytestosend = b''
        self.repeat = 0
        self.timer = None
        mtype = kwargs.pop('mtype')
        # if there is a checksum delete it
        if 'checksum' in kwargs.keys():
            kwargs.pop('checksum')
        if mtype is None:
            raise LCMessageError(f'{mtype} must be present ')
        self.mtype = mtype
        self._payload = Payload()
        if self.mtype.isupper():
            self.lenpayload = -1
            self.set_checksum()
            return
        if self.mtype.islower():
            self.lenpayload = 0
            self.datadict.update(**kwargs)
            self._encodeFromDict()
            self.set_checksum()
        return

    @staticmethod
    def _encodeInteg2bytes(value):
        """ encode an integer in 2 bytes with 7 bits for each byte
        right most bit of byte1 belongs to byte2"""
        if (isinstance(value, int)
                and value >= 0
                and value < 16384
                ):
            # least significant bit of byte1 is to put in bytes2
            left = value // 128
            right = value % 128
            return [left, right]
        else:
            raise LCMessageError(f'in encodeInteg2bytes: {value} is not an integer in O: 16384 range')

    @staticmethod
    def _encodeInteg1byte(value):
        """ encode an integer in 1 byte value"""
        if (isinstance(value, int)
                and value >= 0
                and value < 128
                ):
            byte = value % 128
            return byte
        else:
            raise LCMessageError(f'in encodeInteg1byte: {value} is not an integer in O: 128 range')

    @staticmethod
    def _encodeBits(values, numberofbits):
        """ encode a list of bits into a single value

        @param values : list of binary values (checked calling _encodeBits)
        @type : list

        @param bits : number of bits to encode
        @type : int lower or equal to 7"""
        if isinstance(values, list) and len(values) == numberofbits:
            res = 0
            values.reverse()
            for index, v in enumerate(values, 0):
                res += (2**index) * v
            return res
        else:
            raise LCMessageError(f'cannot encodeBits; value {value} is not a list ')

    @staticmethod
    def _encodebuff(buffer):
        """ encode a padded buffer

        @param buffer : padded buffer
        @type : string"""
        masks = [64, 32, 16, 8, 4, 2, 1]
        result = []
        for b in range(0, int(len(buffer) / 7)):
            byte = 0
            gen = (int(buffer[e + b * 7]) * (masks[e]) for e in range(0, 7))
            for num in gen:
                byte = byte + num
            result.append(byte)
        return result

    @staticmethod
    def _encodeRTLbuff(buffercar):
        """ encode a RTL buffer : the rightmost position
        is the least significant bit of the last transmitted byte

        @param buffercar : list of pusher positions
        @type : string"""
        if len(buffercar) % 7 != 0:
            pad = (7 - len(buffercar) % 7) * '0'
        else:
            pad = ''
        buffer = pad + buffercar
        return LCMessagetoSend._encodebuff(buffer)

    @staticmethod
    def _encodeLTRbuff(buffercar):
        """ encode a LTR buffer: leftmost position
        is the most significant bit of the first byte sent

        @param buffercar : list of pusher positions
        @type : string"""
        if len(buffercar) % 7 != 0:
            pad = (7 - len(buffercar) % 7) * '0'
        else:
            pad = ''
        buffer = buffercar + pad
        return LCMessagetoSend._encodebuff(buffer)

    def _setValue(self, vtype=None, length=None, val=None):
        """returns a value or a list of values for the payload

        @param vtype : type of the value
        @type : string, amongst the keys of the mesvaluetypes dict in common.LCConstants

        @param length : for certain types the length of the value
        @type : integer"""

        if vtype == 'byte':
            value = self._encodeInteg1byte(val)
            self.payload.add(value)
            self.lenpayload += 1
        elif vtype == '2bytes':
            value = self._encodeInteg2bytes(val)
            self.payload.add(value)
            self.lenpayload += 2
        elif vtype == 'strtype':
            for e in val:
                self.payload.add(ord(e))
            self.lenpayload += len(val)
        elif vtype == 'bits':
            self.payload.add(self._encodeBits(val, length))
            self.lenpayload += 1
        elif vtype == 'bool':
            self.payload.add(val)
            self.lenpayload += 1
        elif vtype == 'LTRBuff':
            v = self._encodeLTRbuff(val)
            self.payload.add(v)
            self.lenpayload += (int(len(val) / 7) + 1 * (len(val) % 7 != 0))
        elif vtype == 'RTLBuff':
            self.payload.add(self._encodeRTLbuff(val))
            self.lenpayload += (int(len(val) / 7) + 1 * (len(val) % 7 != 0))
        else:
            raise LCMessageError(f'{self.mtype} has incorrect type definition')
        return

    def _encodeFromDict(self):
        """ fills the payload and lenpayload of the message """

        if self.mtype.isupper() and self.mtype in LCProtocolHelpers.sMessagesDef.keys():
            # single byte message done
            return
        elif self.mtype.islower() and self.mtype in LCProtocolHelpers.sMessagesDef.keys():
            coding = LCProtocolHelpers.sMessagesDef.get(self.mtype)
            # if LTR or RTL buffer need to check lenght versu extreme needles
            if self.mtype == 'l' or self.mtype == 'r':
                self.checklr()
            names = coding.get('values')
            for name in names:
                typedef = LCProtocolHelpers.mesTypesDef.get(name, None)
                if typedef == None:
                    raise LCMessageError(f'{name} has no type definition')
                vtype = typedef.get('vtype')
                extra = typedef.get('extra')
                if extra is not None:
                    send = extra.get('send')
                    if send is not None:
                        extra = send
                length = typedef.get('length')
                # name to look into the datadict
                value = self.datadict.get(name)

                # if value is None, it can be an optional value (only message number in e message)
                if value is not None:

                    if vtype is None:
                        raise LCMessageError(f'incorrect type definition for {name}')
                    # some extras must be applied before encoding
                    if extra is not None:
                        value = self._extra(extra, value)

                    # create payload
                    self._setValue(vtype, length, value)
                else:
                    if not (name == 'mesNumber' and
                            self.mtype == 'e' and not(self.datadict.get('code') in LCProtocolHelpers.eCodesWithMesNum)
                            ):
                        raise LCMessageError(f'{name} is mandatory')
            return
        else:
            # type not found
            raise LCMessageError(f'{self.mtype} is incorrect message type')

    def toDict(self):
        """ returns a dictionnary of the LCMessage including checksum and mtype"""
        if self.lenpayload == 0 or len(self.payload) == 0:

            return {'mtype': self.mtype,
                    'checksum': self.checksum,
                    **self.datadict}

        else:
            return {'mtype': self.mtype,
                    'lenpayload': self.lenpayload,
                    'payload': self.payload,
                    'checksum': self.checksum}


if __name__ == '__main__':

    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__name__', test=True)

    from pathlib import Path
    current = Path(__file__)
    tests_dir = Path(current.parent, 'tests')
    loader = unittest.TestLoader()
    tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)
    


