#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Tests for class LCProtocolMgr : tests Init Fsm transitions

Implements
==========

 tests each transition code  with event containing test data
 checks that test data in event is correct by checking no log critical is found
 functions called in the Fsm transitions are tested in tests_protocol_manager_functions
 tests in isolation of other modules in particular lock_controler_writer, lock_reader
 most functions in protocol manager are moked in autospec to check they are called with correct parameters

Documentation
=============



Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
import unittest
from lock_controler_dialog.LC_message_received import create_from_dic
from lock_controler_dialog.LC_message_tosend import LCMessagetoSend
from lock_controler_dialog.tests.tests_super_class import Test_super_class
from common.tools_for_log import search_in_log
import logging


class TestInitFsmTransitions (Test_super_class, unittest.TestCase):
    """ tests for InitFsm transitions in the order of the code
    test objective only go through each transition and check new state
    other tests suites are targeted towards testing the results of the functions """

    def setUp(self):
        """ TestMainFsmTransitions creates the protocol manager instance and do the appropriate mocks
        changes current Fsm to InitFsm
       """

        super().commonsetUp_for_pm()

        event = {'evName': 'evènement'}
        self.pm.changeFsm('InitFsm', initial='waitingLC', **event)

    def test1_on_msg_in_w(self):
        LCmessage = create_from_dic(self.dataformessages.get('mw'))
        event = LCmessage.createEvent()
        # create a test configuration so it exists
        self.conf = super().get_test_configuration(__file__, forceNew=True)

        with unittest.mock.patch('lock_controler_dialog.protocol_manager.get_configuration', return_value=self.conf):
            self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'hardwareCheck')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list,
                                      'on_msg_in_w', logging.DEBUG))

    def test2_on_max_repeat(self):

        event = {'evName': 'on_max_repeat'}
        currentmessage = self.dictmessages.get('ml1')
        self.pm.currentMess = LCMessagetoSend(**currentmessage)

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'waitingLC')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test3_on_LC_connection_failed(self):

        event = {'evName': 'on_LC_connection_failed'}

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'stop')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, 'fatal failure',
                                      logging.CRITICAL))

    def test4_on_msg_in_d_hardware_OK(self):
        LCmessage = create_from_dic(self.dataformessages.get('md'))
        event = LCmessage.createEvent()
        event['V15'] = 14
        self.pm.changeFsm('InitFsm', 'hardwareCheck')
        conf = super().get_test_configuration(__file__, forceNew=True)
        dic = {'brand': 'PassapE6000 '}
        conf.lockControlerConf.fill(**dic)
        conf.lockControlerConf.update()

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'LCReady')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test5_on_msg_in_d_hardware_NOK(self):
        LCmessage = create_from_dic(self.dataformessages.get('md'))

        event = LCmessage.createEvent()
        event['Vcc'] = 0
        self.pm.changeFsm('InitFsm', 'hardwareCheck')

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'watingUsrHard')

        self.pm.pres.usr_request.assert_called_once()
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test6_on_usr_click_hard(self):

        event = {'evName': 'on_usr_click_hard'}
        self.pm.changeFsm('InitFsm', 'watingUsrHard')

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'hardwareCheck')
        self.assertTrue(self.pm.currentMess.mtype == 'D')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test7_on_int_start_R(self):

        event = {'evName': 'on_int_start_R'}
        self.pm.changeFsm('InitFsm', 'LCready')

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'waitingUserLockPosR')
        self.pm.pres.usr_request.assert_called_once()
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test8_on_int_start_L(self):

        event = {'evName': 'on_int_start_L'}
        self.pm.changeFsm('InitFsm', 'LCready')

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'waitingUserLockPosL')
        self.pm.pres.usr_request.assert_called_once()
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test9_on_usr_click_start_R(self):

        event = {'evName': 'on_usr_click_start'}
        self.pm.changeFsm('InitFsm', 'waitingUserLockPosR')

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'waitingLCAckR')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test10_on_usr_click_start_L(self):

        event = {'evName': 'on_usr_click_start'}
        self.pm.changeFsm('InitFsm', 'waitingUserLockPosL')

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'waitingLCAckL')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test11_on_msg_in_N_R(self):

        event = {'evName': 'on_msg_in_N'}
        self.pm.changeFsm('InitFsm', 'waitingLCAckR')

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'waitingUserLockPosR')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test12_on_msg_in_N_L(self):

        event = {'evName': 'on_msg_in_N'}
        self.pm.changeFsm('InitFsm', 'waitingLCAckL')

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'waitingUserLockPosL')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test13_on_msg_in_A_R(self):

        event = {'evName': 'on_msg_in_A'}
        self.pm.changeFsm('InitFsm', 'waitingLCAckR')

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm, self.pm)
        self.assertEqual(self.pm.currentFsm.state, 'readyKnitTest')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test14_on_msg_in_A_L(self):

        event = {'evName': 'on_msg_in_A'}
        self.pm.changeFsm('InitFsm', 'waitingLCAckL')

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm, self.pm)
        self.assertEqual(self.pm.currentFsm.state, 'readyKnitTest')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def tearDown(self):
        pass


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)

    unittest.main()
