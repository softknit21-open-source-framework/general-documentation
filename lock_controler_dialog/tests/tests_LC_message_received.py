#!/usr/bin/env python
# coding:utf-8
"""
  This file is part of SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

tests for class LCMessageReceived

Implements
==========



Documentation
=============

    
Usage
=====
exceptions raised only for errors from programming environement
errors which might come from transmission on serial port are not raised but logged at critical level

@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


from common. SoftKnit21Errors import LCMessageError
from lock_controler_dialog.LC_Helpers import LCProtocolHelpers
import unittest
from unittest.mock import patch, Mock
from lock_controler_dialog.LC_message_received import LCMessageReceived

debug_test = None


class TestInitLCMessageReceived(unittest.TestCase):
    """ tests for LCMessage class constructor"""

    def setUp(self):
        pass

    def test_errors(self):
        # no mttype
        with self.assertRaises(LCMessageError):
            LCMessageReceived(None, None)
        # lower and wrong lenpayload
            LCMessageReceived('m', 'b')

    def test_init_correct_multibyte_message(self):
        mmu = LCMessageReceived('p', 2)
        self.assertTrue(isinstance(mmu, LCMessageReceived))
        mmu.payload.add([b'\x3f', b'\x02'])
        mmu.set_checksum(b'\x33')
        self.assertTrue(mmu.checksum)
        # test for add to payload
        mmu.payload.add(b'\x55')
        self.assertEqual(mmu.payload, [63, 2, 85])

    def test_init_multibyte_message_incorrect_checksum(self):
        mmu = LCMessageReceived('p', 2)
        self.assertTrue(isinstance(mmu, LCMessageReceived))
        mmu.payload.add([b'\x3f', b'\x02'])
        mmu.set_checksum(b'\x34')
        self.assertFalse(mmu.checksum)

    def test_init_single_byte_message(self):
        msi = LCMessageReceived('A', None)
        self.assertTrue(isinstance(msi, LCMessageReceived))
        self.assertEqual(msi.checksum, None)
        self.assertEqual(msi.payload, [])
        self.assertEqual(msi.lenpayload, -1)

    def test_add_checksum_single_byte_message(self):
        msi = LCMessageReceived('A', None)
        self.assertRaises(LCMessageError, msi.set_checksum, b'\x34')

    def test_isValid_multibyte_message(self):
        mmu = LCMessageReceived('p', 2)
        mmu.payload.add([b'\x3f', b'\x02'])
        mmu.set_checksum(b'\x33')
        self.assertTrue(mmu.isValid())

    def test_isValid_single_byte_message(self):
        msi = LCMessageReceived('A', None)
        self.assertTrue(msi.isValid())

    def test_is_not_valid_multibyte_message(self):
        mmu = LCMessageReceived('p', 3)
        mmu.payload.add([b'\x3f', b'\x02'])
        self.assertFalse(mmu.isValid())

    def test_is_not_valid_single_byte_message(self):
        msi = LCMessageReceived('A', 0)
        msi.lenpayload = 2
        self.assertFalse(msi.isValid())

    def test_is_valid_with_incomplete_message(self):
        m = LCMessageReceived('m', None)
        self.assertFalse(m.isValid())

    def tearDown(self):
        pass


class TestFunctions (unittest.TestCase):
    """ tests functions for createCmesDict"""

    def test_add_payload(self):
        m = LCMessageReceived('m')
        with self.assertRaises(LCMessageError):
            m.payload.add('e')

    def test_decodebits(self):
        bits = '0010 0010'
        data = int('22', 16)
        result = [1, 0, 0, 0, 1, 0]
        self.assertEqual(LCMessageReceived._decodebits(data, 6), result)
        with self.assertRaises(LCMessageError):
            LCMessageReceived._decodebits(255, 6), result
            LCMessageReceived._decodebits(-1, 6), result
            LCMessageReceived._decodebits('a', 6), result

    def test_decode2bytes(self):
        bits = ['001 1011', '111 0010']
        data = [int('1b', 16), int('72', 16)]
        resultbits = ['0000 1101', '1111 0010']
        result = int('0d', 16) * 256 + int('f2', 16)
        self.assertEqual(LCMessageReceived._decode2bytes(data), result)
        with self.assertRaises(LCMessageError):
            LCMessageReceived._decode2bytes([-3, 0])
            LCMessageReceived._decode2bytes([350, 0])
            LCMessageReceived._decode2bytes([100, -2])
            LCMessageReceived._decode2bytes([100, 350])

    def test_decodebytes2_error(self):
        data = {'mtype': 'p', 'lenpayload': 2, 'payload': [b'\x40', b'\x52'], 'checksum': b'\x04'}
        m = LCMessageReceived('p', 2)
        m.set_checksum(data.get('checksum'))
        with self.assertRaises(LCMessageError):
            m._decode2bytes('5')

    def test__decodeLTRBuff(self):
        data = [68, 49, 12]
        expected = '100 0100 011 0001 000 1100'
        self.assertEqual(LCMessageReceived._decodeLTRBuff(data), expected.replace(' ', ''))

    def test__decodeRTLBuff(self):
        data = [68, 49, 12]
        expected = '100 0100 011 0001 000 1100'
        self.assertEqual(LCMessageReceived._decodeRTLBuff(data), expected.replace(' ', ''))

    def test_set_checksum(self):
        m = LCMessageReceived('A')
        self.assertRaises(LCMessageError, m.set_checksum, 3)
        m = LCMessageReceived('e')
        m.payload.add([10, 15])
        result = (ord('e') + 2 + 10 + 15) % 128
        m.lenpayload = 2
        m.set_checksum(result)
        self.assertTrue(m.checksum)
        m.set_checksum(result + 1)
        self.assertFalse(m.checksum)

    def test_isValid(self):
        m = LCMessageReceived('A')
        self.assertTrue(m.isValid())
        m = LCMessageReceived('e')
        m.payload.add([10, 15])
        result = (ord('e') + 2 + 10 + 15) % 128
        m.lenpayload = 2
        m.set_checksum(result)
        self.assertTrue(m.isValid())
        m._payload = None
        self.assertFalse(m.isValid())
        m._payload = 1
        self.assertFalse(m.isValid())
        m.mtype = 'x'
        self.assertFalse(m.isValid())


class TestCreateMessDictforMessages(unittest.TestCase):
    """ tests createMesdict for all messages"""

    def test_message_a(self):
        m = LCMessageReceived('a')
        m.payload.add([123])
        m.lenpayload = 1
        res = (ord('a') + 1 + 123) % 128
        m.set_checksum(res)
        res = m.createLCmesDict()
        expected = {'class': 'normal', 'mesNumber': 123}
        self.assertEqual(m.datadict, expected)
        self.assertTrue(m.checksum)

    def test_message_n(self):
        m = LCMessageReceived('n')
        m.payload.add([123])
        m.lenpayload = 1
        res = (ord('n') + 1 + 123) % 128
        m.set_checksum(res)
        res = m.createLCmesDict()
        expected = {'class': 'normal', 'mesNumber': 123}
        self.assertEqual(m.datadict, expected)
        self.assertTrue(m.checksum)

    def test_message_d(self):
        m = LCMessageReceived('d')
        m.payload.add([0, 55, 1, 5])
        m.lenpayload = 4
        res = (ord('d') + 4 + sum(m.payload)) % 128
        m.set_checksum(res)
        res = m.createLCmesDict()
        expected = {'class': 'normal', 'Vcc': 5.5, 'V15': 13.3}
        self.assertEqual(m.datadict, expected)
        self.assertTrue(m.checksum)

    def test_message_w(self):
        m = LCMessageReceived('w')
        m.payload.add([80, 97, 115, 115, 97, 112, 69, 54, 48, 48, 48, 32, 76, 67, 49, 53, 50, 48, 49])
        m.lenpayload = 19
        res = (ord('w') + 19 + sum(m.payload)) % 128
        m.set_checksum(res)
        res = m.createLCmesDict()
        expected = {'class': 'normal', 'brand': 'PassapE6000 ',
                    'hard': 'LC15',
                    'soft': '201'}
        self.assertEqual(m.datadict, expected)
        self.assertTrue(m.checksum)

    def test_message_p(self):
        m = LCMessageReceived('p')
        m.payload.add([63, 2])
        m.lenpayload = 2
        res = (ord('p') + 2 + sum(m.payload)) % 128
        m.set_checksum(res)
        res = m.createLCmesDict()
        expected = {'class': 'test', 'lockpos': -126}
        self.assertEqual(m.datadict, expected)
        self.assertTrue(m.checksum)

    def test_message_s(self):
        m = LCMessageReceived('s')
        m.payload.add([51])
        m.lenpayload = 1
        res = (ord('s') + 1 + sum(m.payload)) % 128
        m.set_checksum(res)
        res = m.createLCmesDict()
        expected = {'class': 'test', 'PrevDir': LCProtocolHelpers.RTL,
                    'PrevCsense': 1,
                    'PrevCref': 0,
                    'Dir': LCProtocolHelpers.LTR,
                    'CSense': 1,
                    'Cref': 1}
        self.assertEqual(m.datadict, expected)
        self.assertTrue(m.checksum)

    def test_message_t(self):
        m = LCMessageReceived('t')
        # length of buffer with no extra bits
        m.payload.add([1, 20, 108, 63, 68, 63, 88, 68, 49, 12])
        m.lenpayload = 10
        res = (ord('t') + m.lenpayload + sum(m.payload)) % 128
        m.set_checksum(res)
        res = m.createLCmesDict()
        expected = {'class': 'test', 'buffindex': 1, 'mesNumber': 20, 'bufftype': 'l',
                    'leftmost': 30, 'rightmost': 50, 'LTRBuffer': '100010001100010001100'}
        self.assertEqual(m.datadict, expected)
        self.assertTrue(m.checksum)
        # length of buffer with  extra bits
        m = LCMessageReceived('t')
        m.payload.add([1, 20, 108, 63, 38, 63, 60, 66, 8, 48, 64])
        m.lenpayload = len(m.payload)
        res = (ord('t') + m.lenpayload + sum(m.payload)) % 128
        m.set_checksum(res)
        res = m.createLCmesDict()
        buff = '100 0010 000 1000 011 0000 100 0000'
        expected = {'class': 'test', 'buffindex': 1, 'mesNumber': 20, 'bufftype': 'l',
                    'leftmost': 0, 'rightmost': 22, }
        lenbuff = expected['rightmost'] - expected['leftmost'] + 1
        expected.update({'LTRBuffer': buff.replace(' ', '')[0:lenbuff]})
        self.assertEqual(m.datadict, expected)
        self.assertTrue(m.checksum)


class TestCreateMesdictforAllMessages (unittest.TestCase):
    """ tests createMesdict for all test messages"""

    def setUp(self):
        from lock_controler_dialog.tests.data.data_for_LCMessages_Tests import dataformessages, datadictmessages
        # data for all types of messages
        self.indata = dataformessages

        # output of test for messages
        self.datadict = datadictmessages

        # for better test coverage some functions are tested on send messages
        # mock the messages definition to include sent messages
        self.totalMesDef = LCProtocolHelpers.rMessagesDef
        self.totalMesDef.update(LCProtocolHelpers.sMessagesDef)
        patcher = patch('lock_controler_dialog.LC_message_received.LCProtocolHelpers.rMessagesDef', self.totalMesDef)
        self.mockmesdef = patcher.start()
        self.addCleanup(patcher.stop)

    def test_createLCMesDict_all_test_messages(self):
        for key, message in self.indata.items():
            if debug_test:
                print('key', key)
            m = LCMessageReceived(message.get('mtype'), message.get('lenpayload'))
            m.payload.add(message.get('payload'))
            if m.mtype.islower():
                m.set_checksum(message.get('checksum'))
            res = m.createLCmesDict()
            res.update({'mtype': m.mtype, 'checksum': m.checksum})
            expected = self.datadict.get(key)
            self.assertEqual(res, expected)

    def test_create_event_error_checksum(self):
        data = {'mtype': 'e', 'lenpayload': 2, 'payload': [b'\x14', b'\x78', ], 'checksum': b'\x73'}
        me = LCMessageReceived('e', 2)
        me.payload.add(data.get('payload'))
        me.set_checksum(b'\x75')
        self.assertRaises(LCMessageError, me.createEvent)

    def test_create_Event_e_message(self):
        """test creation of event for e message"""
        data = {'mtype': 'e', 'lenpayload': 2, 'payload': [b'\x14', b'\x78', ], 'checksum': b'\x73'}
        expected = {'mtype': 'e', 'class': 'normal', 'errorcode': 20, 'mesNumber': 120, 'checksum': True}
        me = LCMessageReceived('e', 2)
        me.payload.add(data.get('payload'))
        me.set_checksum(data.get('checksum'))
        res = me.createEvent()
        self.assertEqual(res.get('evName'), 'on_msg_in_e_20')
        del res['evName']
        self.assertEqual(res, expected)

        data = self.indata.get('me2')
        me = LCMessageReceived(data.get('mtype'), data.get('lenpayload'))
        me.payload.add(data.get('payload'))
        me.set_checksum(data.get('checksum'))
        res = me.createEvent()
        self.assertEqual(res.get('evName'), 'on_msg_in_e_1*')
        del res['evName']
        expected = {'mtype': 'e', 'class': 'normal', 'errorcode': 10, 'mesNumber': None, 'checksum': True}
        self.assertEqual(res, expected)

    def test_create_Event_e_message_with_error(self):
        """test creation of event for e message with error"""
        data = {'mtype': 'e', 'lenpayload': 2, 'payload': [b'\x14', b'\x78', ], 'checksum': b'\x73'}
        expected = {'mtype': 'e', 'class': 'normal', 'errorcode': 20, 'mesNumber': 120, 'checksum': True}
        me = LCMessageReceived('e', 2)
        me.lenpayload = 2
        # wrong error code
        me.payload.add([b'\x15', b'\x78'])
        me.set_checksum(data.get('checksum'))
        self.assertRaises(LCMessageError, me.createEvent)
        me = LCMessageReceived('e', 2)
        me.lenpayload = 2
        # no error code
        me.payload.add([b'\x18'])
        self.assertRaises(LCMessageError, me.createEvent)

    def test_create_Event_w_message(self):
        data = {'mtype': 'w', 'lenpayload': 19, 'payload': [b'\x50', b'\x61', b'\x73', b'\x73', b'\x61',
                                                            b'\x70', b'\x45', b'\x36', b'\x30', b'\x30',
                                                            b'\x30', b'\x20', b'\x4c', b'\x43', b'\x31',
                                                            b'\x35', b'\x32', b'\x30', b'\x31'],
                'checksum': b'\x25'}
        mw = LCMessageReceived(data.get('mtype'), data.get('lenpayload'))
        mw.payload.add(data.get('payload'))
        mw.set_checksum(data.get('checksum'))
        res = mw.createEvent()
        self.assertEqual(res.get('evName'), 'on_msg_in_w')
        expected = {'mtype': 'w', 'class': 'normal', 'brand': 'PassapE6000 ',
                    'hard': 'LC15',
                    'soft': '201',
                    'checksum': True}
        del res['evName']
        self.assertEqual(res, expected)

    def test_create_Event_w_message_with_error(self):
        """checks the error return from check brand and soft and hard in configuration manager"""
        data = {'mtype': 'w', 'lenpayload': 19, 'payload': [b'\x50', b'\x61', b'\x73', b'\x73', b'\x61',
                                                            b'\x70', b'\x45', b'\x36', b'\x30', b'\x30',
                                                            b'\x30', b'\x20', b'\x4c', b'\x43', b'\x31',
                                                            b'\x35', b'\x32', b'\x30', b'\x31'],
                'checksum': b'\x25'}
        mw = LCMessageReceived(data.get('mtype'), data.get('lenpayload') - 1)
        wrongBrandPayload = [b'\x50', b'\x61', b'\x73', b'\x61',
                             b'\x70', b'\x45', b'\x36', b'\x30', b'\x30',
                             b'\x30', b'\x20', b'\x4c', b'\x43', b'\x31',
                             b'\x35', b'\x32', b'\x30', b'\x31']
        mw.payload.add(wrongBrandPayload)
        mw.set_checksum(data.get('checksum'))
        self.assertRaises(LCMessageError, mw.createEvent)
        wrongLCPayload = [b'\x50', b'\x61', b'\x73', b'\x73', b'\x61',
                          b'\x70', b'\x45', b'\x36', b'\x30', b'\x30',
                          b'\x30', b'\x20', b'\x4c', b'\x31',
                          b'\x35', b'\x32', b'\x30', b'\x31']
        mw.payload.add(wrongLCPayload)
        mw.set_checksum(data.get('checksum'))
        self.assertRaises(LCMessageError, mw.createEvent)
        wrongsoftPayload = [b'\x50', b'\x61', b'\x73', b'\x73', b'\x61',
                            b'\x70', b'\x45', b'\x36', b'\x30', b'\x30',
                            b'\x30', b'\x20', b'\x4c', b'\x43', b'\x31',
                            b'\x35', b'\x32', b'\x31']
        mw.payload.add(wrongsoftPayload)
        mw.set_checksum(data.get('checksum'))
        self.assertRaises(LCMessageError, mw.createEvent)

    def test_create_event_A_message(self):
        data = {'mtype': 'A', 'lenpayload': -1, 'payload': []}
        mA = LCMessageReceived(data.get('mtype'), data.get('lenpayload'))
        res = mA.createEvent()
        self.assertEqual(res.get('evName'), 'on_msg_in_A')
        del res['evName']
        expected = {'mtype': 'A', 'class': 'normal', 'checksum': None}
        self.assertEqual(res, expected)

    def tearDown(self):
        pass


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)
    unittest.main()

