#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

tests for class LCMessagetoSend

Implements
==========

 - B{}

Documentation
=============

    

Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
from lock_controler_dialog.LC_message_tosend import LCMessagetoSend
from lock_controler_dialog.LC_Helpers import LCProtocolHelpers
from common. SoftKnit21Errors import LCMessageError


import unittest
from unittest.mock import patch, Mock


class TestFunctions (unittest.TestCase):
    def setUp(self):
        pass

    def test_check_lr(self):
        data = {'mtype': 'l', 'mesNumber': 3, 'leftmost': 50, 'rightmost': 100, 'LTRBuffer': 51 * '1'}
        m = LCMessagetoSend(**data)
        m.checklr()
        m.datadict.update({'leftmost': 10, 'rightmost': 20, 'LTRBuffer': 12 * '1'})
        self.assertRaises(LCMessageError, m.checklr)
        m.mtype = 'l'
        self.assertRaises(LCMessageError, m.checklr)

    def test_func_encodeLTRbuff(self):
        data = '100 1000 110 0100 1'
        expected = [72, 100, 64]
        self.assertEqual(LCMessagetoSend._encodeLTRbuff(data.replace(' ', '')), expected)
        data = '100 1'
        expected = [72]
        self.assertEqual(LCMessagetoSend._encodeLTRbuff(data.replace(' ', '')), expected)
        data = '100 1000 110 0100 110 1011'
        expected = [72, 100, 107]
        self.assertEqual(LCMessagetoSend._encodeLTRbuff(data.replace(' ', '')), expected)

    def test_encodeBits(self):
        import sys
        data = [1, 1, 0, 0, 1, 1]
        result = int('33', 16)
        self.assertEqual(LCMessagetoSend._encodeBits(data, 6), result)

    def test_func_encodeRTLbuff(self):
        data = '1 000 0000 100 0000 010 0000'
        result = [1, 0, 64, 32]
        self.assertEqual(LCMessagetoSend._encodeRTLbuff(data.replace(' ', '')), result)
        data = '10 1011 000 0000 100 0000 010 0000'
        result = [43, 0, 64, 32]
        self.assertEqual(LCMessagetoSend._encodeRTLbuff(data.replace(' ', '')), result)
        data = '110 1011 000 0000 100 0000 010 0000'
        result = [107, 0, 64, 32]
        self.assertEqual(LCMessagetoSend._encodeRTLbuff(data.replace(' ', '')), result)
        data = '10001'
        result = [17]
        self.assertEqual(LCMessagetoSend._encodeRTLbuff(data.replace(' ', '')), result)

    def test_func_encodeInteg1byte(self):
        data = 125
        self.assertEqual(LCMessagetoSend._encodeInteg1byte(data), 125)
        with self.assertRaises(LCMessageError) as cm:
            LCMessagetoSend._encodeInteg1byte(-1)
            LCMessagetoSend._encodeInteg1byte(150)

    def test_func_encodeInteg2bytes(self):
        data = int('f0', 16)
        result = [1, 112]
        self.assertEqual(LCMessagetoSend._encodeInteg2bytes(data), result)
        with self.assertRaises(LCMessageError) as cm:
            LCMessagetoSend._encodeInteg1byte(-1)
            LCMessagetoSend._encodeInteg1byte(260)

    def test_set_checksum(self):
        dic = {'mtype': 'D'}
        m = LCMessagetoSend(**dic)
        self.assertTrue(m.checksum is None)
        dic = {'mtype': 'x', 'bufftype': 'l', 'mesNumber': 3}
        m = LCMessagetoSend(**dic)
        result = (ord('x') + 2 + ord('l') + 3) % 128
        self.assertEqual(result, m.checksum)


class TestMessagestoSend(unittest.TestCase):

    def test_create_x(self):
        dic = {'mtype': 'x', 'bufftype': 'l', 'mesNumber': 3}
        payload = [ord('l'), 3]
        m = LCMessagetoSend(**dic)
        self.assertEqual(m.lenpayload, 2)
        self.assertEqual(m.payload, payload)

    def test_create_l(self):
        buffercar = '100 0100 011 0001 000 1100 00'
        dic = {'mtype': 'l', 'class': 'normal', 'mesNumber': 1, 'leftmost': 30, 'rightmost': 52,
               'LTRBuffer': buffercar.replace(' ', '')}
        m = LCMessagetoSend(**dic)
        payload = [1]
        payload.extend(m._encodeInteg2bytes(LCProtocolHelpers.get_LClockpos(30)))
        payload.extend(m._encodeInteg2bytes(LCProtocolHelpers.get_LClockpos(52)))
        payload.extend(m._encodeLTRbuff(buffercar.replace(' ', '')))
        self.assertEqual(m.payload, payload)

    def test_create_r(self):
        buffercar = '100 0100 011 0001 000 1100 00'
        dic = {'mtype': 'r', 'class': 'normal', 'mesNumber': 1, 'leftmost': 30, 'rightmost': 52,
               'RTLBuffer': buffercar.replace(' ', '')}
        m = LCMessagetoSend(**dic)
        payload = [1]
        payload.extend(m._encodeInteg2bytes(LCProtocolHelpers.get_LClockpos(30)))
        payload.extend(m._encodeInteg2bytes(LCProtocolHelpers.get_LClockpos(52)))
        payload.extend(m._encodeRTLbuff(buffercar.replace(' ', '')))
        self.assertEqual(m.payload, payload)

    def test_creat_r_error(self):
        """test r message with error in checkregexp"""
        buffercar = '100 0100 011 0A01 000 1100 00'
        dic = {'mtype': 'r', 'class': 'normal', 'mesNumber': 1, 'leftmost': 30, 'rightmost': 52,
               'RTLBuffer': buffercar.replace(' ', '')}
        with self.assertRaises(LCMessageError):
            m = LCMessagetoSend(**dic)


class TestCreateLCMessageAll(unittest.TestCase):
    """ tests from dictionnary of tests messages
    to test all lines of mesTypdef"""

    def setUp(self):

        from lock_controler_dialog.tests.data.data_for_LCMessages_Tests import dataformessages, datadictmessages
        self.inputd = datadictmessages

        # values to be obtained in the result dictionnary
        self.expected = dataformessages

        # all messages are tested including received messages
        totaldict = LCProtocolHelpers.sMessagesDef
        totaldict.update(LCProtocolHelpers.rMessagesDef)
        patcher3 = patch('lock_controler_dialog.LC_Helpers.LCProtocolHelpers.sMessagesDef',
                         totaldict)
        self.mock = patcher3.start()
        self.addCleanup(patcher3.stop)

    def test_init_correct_messages(self):
        for key, dicti in self.inputd.items():
            if key != 'mp2':

                a = LCMessagetoSend(**dicti)
                res = {'mtype': a.mtype,
                       'lenpayload': a.lenpayload,
                       'payload': a.payload,
                       'checksum': a.checksum}
                mexpected = self.expected.get(key)
                self.assertTrue(res, mexpected)

    def test_extra_functions_errors_in_message_Abstract(self):

        inputWithError = {
            # islist
            'es': {'mtype': 's', 'class': 'test', 'PrevDir': LCProtocolHelpers.RTL,
                   'PrevCsense': 2,
                   'PrevCref': 0,
                   'Dir': LCProtocolHelpers.LTR,
                   'CSense': 1,
                   'Cref': 1,
                   'checksum': True},
            # checklist
            'eeb': {'mtype': 'e', 'class': 'normal', 'errorcode': 21, 'checksum': True},
            # checkconf
            'mw': {'mtype': 'w', 'class': 'normal', 'brand': 'singer ',
                   'hard': 'LC15',
                   'soft': '201',
                   'checksum': True},
            # checkregexp
            'ml1': {'mtype': 'l', 'class': 'normal', 'mesNumber': 1, 'leftmost': -20, 'rightmost': 1,
                    'LTRBuffer': '10001BC0001100010001100',
                    'checksum': True},
            # error in lockkpos
            'mp2': {'mtype': 'p', 'class': 'test', 'lockpos': -126, 'checksum': True},

        }

        for key, dicti in inputWithError.items():
            with self.assertRaises(LCMessageError):
                LCMessagetoSend(**dicti)

    def test_other_errors_messages(self):
        inputWithError = {
            # no mtype
            'ema': {'mtype': None, 'class': 'normal', 'mesNumber': 20, 'checksum': True},
            # no typedef for locks
            'emp': {'mtype': 'p', 'class': 'test', 'locks': 82, 'checksum': True},
            # wrong name V
            'emd': {'mtype': 'd', 'class': 'normal', 'Vcc': 5.5, 'V': 13.3, 'checksum': True},
            # integerbytes over 156 for an optional field
            'ea': {'mtype': 'a', 'class': 'normal', 'mesNumber': 156, 'checksum': True},
            # intger2 bytes toobig
            'ep': {'mtype': 'p', 'class': 'test', 'lockpos': 12150, 'checksum': True},
            # missing argument
            'ema2': {'mtype': 'a', 'class': 'normal', 'checksum': True},
            # wrong type
            'mw': {'mtype': 'z', 'class': 'normal', 'brand': 'PassapE6000 ',
                   'hard': 'LC15',
                           'soft': '201', }
        }

        for key, dicti in inputWithError.items():
            with self.assertRaises(LCMessageError):
                LCMessagetoSend(**dicti)

        dicti = {'mtype': 'l', 'class': 'normal', 'mesNumber': 1, 'leftmost': -20, 'rightmost': 1,
                 'LTRBuffer': '100010001100010001100',
                 'checksum': True}
        # no vtype
        with patch.dict('lock_controler_dialog.LC_message_tosend.LCProtocolHelpers.mesTypesDef',
                        {'LTRBuffer': {'vtype': 'LTRf', 'length': None,
                                       'extra': {'send': {'checkregexp': '^[0-1]+'},
                                                 'receive': {'sliceO': None}}}}):
                with self.assertRaises(LCMessageError):
                    LCMessagetoSend(**dicti)


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)
    unittest.main()

    #import unittest

    #from common.tools_for_log import logconf

    #mlog = logconf('__name__', test=True)

    #from pathlib import Path
    #current = Path(__file__)
    #tests_dir = Path(current.parent, 'tests')
    #loader = unittest.TestLoader()
    #tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    #testRunner = unittest.runner.TextTestRunner()
    # testRunner.run(tests_suite)

