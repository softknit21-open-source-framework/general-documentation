#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Test for class LCProtocolMgr : second part of the tests for the methods

Implements
==========

Documentation
=============

Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
import unittest

from lock_controler_dialog.LC_Helpers import LCProtocolHelpers
from lock_controler_dialog.LC_message_tosend import LCMessagetoSend
from lock_controler_dialog.tests.tests_super_class import Test_super_class

import json
import logging


class Test_functions_Buffer_related(Test_super_class, unittest.TestCase):

    def setUp(self):

        super().commonsetUp_for_pm()

    def test1_onErrorBufferMess_message_found(self):
        """ test repeat_write called and log critical"""
        eventmessage = self.dictmessages.get('me')
        eventmessage['error_code'] = '4*'
        messnum = eventmessage['mesNumber']
        currentmessage = self.dictmessages.get('ml1')
        self.pm.currentMess = LCMessagetoSend(**currentmessage)
        self.pm.currentMess.datadict['mesNumber'] = messnum
        message = self.pm.currentMess
        event = {'evName': 'on_msg_in_e_4*', **eventmessage}

        self.pm.errorBufferMess(**event)

        self.assertEqual(self.pm.currentMess, message)
        (LCmessage, timeout), emptydic = self.pm.LC.repeat_write.call_args_list[0]
        self.assertEqual(LCmessage, message)
        self.assertEqual(timeout, LCProtocolHelpers.TIMEOUT_MESSAGE)
        (level, lmessage), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.CRITICAL)
        self.assertTrue(f'''message {event['mesNumber']}''' in lmessage)

    def test2_ErrorBufferMess_repeat_False(self):
        """ test repeat_write not called and log critical"""
        eventmessage = self.dictmessages.get('me')
        eventmessage['error_code'] = '4*'
        messnum = eventmessage['mesNumber']
        currentmessage = self.dictmessages.get('ml1')
        self.pm.currentMess = LCMessagetoSend(**currentmessage)
        self.pm.currentMess.datadict['mesNumber'] = messnum
        message = self.pm.currentMess
        event = {'evName': 'on_msg_in_e_4*', **eventmessage}

        self.pm.errorBufferMess(False, **event)

        self.assertEqual(self.pm.currentMess, message)
        self.pm.LC.repeat_write.assert_not_called()
        (level, lmessage), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.CRITICAL)
        self.assertTrue(f'error 4*' in lmessage)

    def test3_onErrorBufferMess_message_number_not_found(self):
        """ test repeat_write not called and log critical"""
        eventmessage = self.dictmessages.get('me')
        eventmessage['error_code'] = '4*'
        currentmessage = self.dictmessages.get('ml1')
        self.pm.currentMess = LCMessagetoSend(**currentmessage)
        event = {'evName': 'on_msg_in_e_4*', **eventmessage}

        self.pm.errorBufferMess(**event)

        self.pm.LC.repeat_write.assert_not_called()
        (level, lmessage), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.CRITICAL)
        self.assertTrue(f'error 4*' in lmessage)

    def test4_ErrorBufferMess_message_wrong_message_type(self):
        """ test repeat_write not called and log critical"""
        eventmessage = self.dictmessages.get('me')
        eventmessage['error_code'] = '4*'
        messnum = eventmessage['mesNumber']
        currentmessage = self.dictmessages.get('ml1')
        self.pm.currentMess = LCMessagetoSend(**currentmessage)
        self.pm.currentMess.datadict['mesNumber'] = messnum
        self.pm.currentMess.mtype = 'p'
        event = {'evName': 'on_msg_in_e_4*', **eventmessage}

        self.pm.errorBufferMess(**event)

        self.pm.LC.repeat_write.assert_not_called()
        (level, lmessage), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.CRITICAL)
        self.assertTrue(f'error 4*' in lmessage)

    def test5_NextBuffer_not_max(self):
        """ test messageNumber incremented modulo 128, message sent
        LCBuffers incremented and log Debug , """
        nexCP = ((85, 105, '1' * 7 + '0' * 7 + '1' * 7, 'r'),)
        self.pm.running_knit.nextCP.side_effect = nexCP
        self.pm.messageNumber = 127
        event = {}

        self.pm.nextBuffer(**event)

        self.assertEqual(self.pm.LCBuffers, 1)
        self.assertEqual(self.pm.messageNumber, 0)
        self.assertEqual(self.pm.currentMess.mtype, 'r')
        (message, timout), emptydic = self.pm.LC.write_message.call_args_list[0]
        self.assertEqual(message, self.pm.currentMess)
        self.assertEqual(timout, LCProtocolHelpers.TIMEOUT_MESSAGE)
        (level, messdic), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.DEBUG)
        logdic = json.loads(str(messdic))
        re = {key: value for key, value in logdic.items() if (key, value) in self.pm.currentMess.toDict().items()}
        self.assertEqual(re, self.pm.currentMess.toDict())

    def test6_NextBuffer_max_buffers(self):
        """ test messageNumber not incremented ,
        LCBuffers not incremented and no log ,wtite message not called, """
        self.pm.LCBuffers = LCProtocolHelpers.NB_MAX_BUFFERS
        currentmess = self.pm.messageNumber
        event = {}

        self.pm.nextBuffer(**event)

        self.assertEqual(self.pm.LCBuffers, LCProtocolHelpers.NB_MAX_BUFFERS)
        self.assertEqual(self.pm.messageNumber, currentmess)
        self.pm.LC.write_message.assert_not_called

# def test7_create_write_message_noRepeat(self):
    # """test message created is correct, message sent with timeOut
    # repeat is the new event to create, og of sent message but not event"""
    #event = {'evName': 'normal'}
    #timeO = 2

    #self.pm.create_write_message_noRepeat('W', timeout=timeO, **event)

    #self.assertTrue(isinstance(self.pm.currentMess, LCMessagetoSend))
    #self.assertEqual(self.pm.currentMess.mtype, 'W')
    # self.mocktimer_cancel.assert_called_once
    #(message, timout), emptydic = self.pm.LC.write_message.call_args_list[0]
    #self.assertEqual(message, self.pm.currentMess)
    #self.assertEqual(timout, timeO)
    #self.assertEqual(self.pm.currentMess.repeat, 'on_time_out_W')
    #(level, evdic), emptydic = self.mocklog.log.call_args_list[0]
    #self.assertEqual(level, logging.DEBUG)
    #logdic = json.loads(str(evdic))
    #re = {key: value for key, value in logdic.items() if (key, value) in self.pm.currentMess.toDict().items()}
    #self.assertEqual(re, self.pm.currentMess.toDict())


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)

    unittest.main()
