#!/usr/bin/env python
# coding:utf-8
"""  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

tests for configuration Management

Implements
==========

 - B{test for configuration manager}

Documentation
=============
 tests for the module configuration manager
Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""

from pathlib import Path

import unittest


class Test_super_class (unittest.TestCase):

    def get_tests_datadir(self, callingfile):
        """ returns the PosixPath for the data file for the tests_script"""

        script = Path(callingfile).stem
        callingfile = Path(callingfile)
        if 'tests' in callingfile.parts:
            filepath = Path(*callingfile.parts[0:-1], 'data', script)
        else:
            filepath = Path(*callingfile.parts[0:-1], 'tests', 'data', script)
        return filepath


