
#!/usr/bin/env python
# coding:utf-8

""" this file is part of SoftKnit21 framework
  
License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Grid pattern storage

Implements
==========

 - B{grid_pattern_storage}

Documentation
=============

    contains functions to read  a gridPattern in local storage 
    to decode gridpattern from different coding schemas

Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


from SoftKnit21.commons.configuration_manager import get_configuration
#from commons.SoftKnit21Errors import GridPatternError
from gettext import gettext as _
from pathlib import Path
from inspect import getmodulename


class PassapCutDecoder:
    
    """ a decoder of gridpatterns in cut format with no palette preceding the description of the grid.
    information were taken from http://www.textfiles.com/programming/FORMATS/pix_fmt.txt
    in complement reverse engineering was done from simple files
    returns the grid or an error message

    format is headers of 6 bytes (2 long integers (2 bytes)  for width and Height and a thide dummy long integer
    then line length in a long integer
    then each line is coded as described in the document with a 0 at the end of each line
    simple files reverse engineering lead to ambiguity :
    either the line length i in a long integer and the 0 for the end of line is included in the line
    or the line length is on a short integer and the 0 end of line is outside of the line
    the first hypothesis seemed more coherent and is implemented in the function
    decoding a gridpattern with line length over 256 would allow to definitely leave ambiguïty
    color code is 255 for a blank square and 0 for a black square

    this decoding algorithm does not seem to work for E8000 gridpatterns : test failed on grid 2195 of E8000

    @ param : file path
    @ type : Path    
    """

    def __init__(self, file):
        self.file = file
        
    @staticmethod
    def color_code(num, index):
        """return the color number in the form of a string
        in Passap cut files, 255 is the white and 0 is black"""
        if num == 255:
            return '0'

        if num == 0:
            return '1'
        else:
            raise ValueError(
                _(f'in module {getmodulename(__file__)} error in decoding gridPattern color {num} on  line {index}; expected color is either 255 for white or 0 for black '))
        
    def genforHeader(self):
        for i in range(0, 3):
            b = self.file.read(2)
            hinteger = int.from_bytes(b, byteorder='little')
            # width and height cannot be zero
            if hinteger <= 0 and i != 2:
                raise ValueError(
                    _(f' in module {getmodulename(__file__)} error in decoding of file {self.file} Header'))
            yield hinteger

    def genforLine(self):
        b = self.file.read(2)
        while b:
            line_length = int.from_bytes(b, byteorder='little')
            line = self.file.read(line_length)
            b = self.file.read(2)
            yield line


    def genforLinecontent(self, line_codes, index):
        i = 0
        while i < len(line_codes):
            count = line_codes[i]
            # end of line is 0
            if i == len(line_codes) - 1:
                if count != 0:
                    raise ValueError(
                        _(f'in module {getmodulename(__file__)} error in decoding file {file} line {index} does not terminate with 0 ')
                    )
                else:
                    break

            if count > 127:
                # most significant bit is 1; it is a repeat factor
                count = count - 128
                byte = line_codes[i + 1]
                i += 2
                y = ''.join([self.color_code(byte, index)] * count)
                yield y

            else:
                # most significant bit is 0 , it is a count of bytes
                codes = line_codes[i + 1: i + count + 1]
                i += count + 1
                y = ''.join([self.color_code(byte, index) for byte in codes])
                yield y
                
    def read_file(self):
        try:
            self.width, self.height, dummy = list(self.genforHeader())
            c = iter(self.genforLine())
            self.line_list = []
            for index, l in enumerate(c):
                a = iter(self.genforLinecontent(l, index))
                b = ''.join(list(a))
                self.line_list.append(b)
    
        except IOError:
            raise ValueError(
                _(f' in module {getmodulename(__file__)} I/O Error in reading gridPattern  file {self.file}')
            )
        self.line_list = self.normalize()
        return self.width, self.height, self.line_list

    def normalize(self):
        return self.line_list
    
        

class ColorNumCutDecoder (PassapCutDecoder):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.colors = []
        

    def color_code(self, num, index):
        """return the color number in the form of a string"""
        if not num in self.colors:
            self.colors.append(num)
        return str(num)
    
    def normalize(self):
        """if the line_list comprises only 2 colors change the format to get 0 and 1"""
        if len(self.colors) == 2 and min(self.colors) == 1:
            new_line_list = []
            for index, line_list in enumerate(self.line_list):
                new_line = ''.join([str(int(s) - 1) for s in line_list])
                new_line_list.append(new_line)
            self.line_list = new_line_list
            return self.line_list



def decodeGridPatternCut(file, typ=None):
    """decode a Cut gridPattern file"""
    
    if not typ:
        decoder = PassapCutDecoder(file)
    elif typ == 'colorNumber':
        decoder = ColorNumCutDecoder(file)
        
    
    if not hasattr(file, 'read'):
        file = open(file, 'rb')
    try:
        width, height, line_list = decoder.read_file()
    except ValueError as ex:
        file.close()   

    if len(line_list) != height:
        raise ValueError(
            _(f'in module {getmodulename(__file__)} error in decoding {file}:  height is {height} but file does contain such number of lines')
        )

    for index, line in enumerate(line_list):
        if len(line) != width:
            raise ValueError(
                _(f'in module {getmodulename(__file__)} error in decoding {file} width is {width} line {index} has wrong length')
            )

    return width, height, line_list


def getGridPatternPath(**kwargs):
    """ returns the directory to access the gridpattern files

    @keywordparam : number
    @type : presently only number of gridpattern
    
    @keywordparam : origin 
    @ type : string; presently only E6000
    """

    config = get_configuration()
    number = kwargs.get('number', None)
    origin = kwargs.get('origin', 'E6000')

    if number == None:
        raise FileNotFoundError(
            _(' error in getting cutfile : this version of SoftKnit21 allows access to GridPattern only with its number'))
    dirpath = config.pathsConfig.getGridPatternsDir(origin)
    if dirpath == None:
        raise ValueError(
            _(f'error in getting cutfile; this version of SoftKnit21 only allows to access E6000 gridPatterns with their number')
        )
    else:
        filepath = dirpath / (str(number) + '.CUT')

    return number, filepath


def getGridPattern(**kwargs):
    number, file = getGridPatternPath(**kwargs)
    return number, *decodeGridPatternCut(file)


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__name__', test=True)

    from pathlib import Path
    current = Path(__file__)
    tests_dir = Path(current.parent, 'tests')
    loader = unittest.TestLoader()
    tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)
