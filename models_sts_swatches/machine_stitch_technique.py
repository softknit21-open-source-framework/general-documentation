#!/usr/bin/env python
# coding:utf-8
"""

 This file is part of SoftKnit21 framework

License
=======

 - B{SofKnit21} (U{http://www.softkni21.org}) is Copyright:
  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the Affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============
contains classes describing a machine stitch technique
Implements
==========


Usage
=====
instances of machine stitch_techniques are created by xml decoder and reading from database
Documentation
=============


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL

"""

import xmlschema as xs


import models_sts_swatches.helpers as helpers

class DMachineSTModelConverter (xs.DataBindingConverter):
    """DataBindingConverter for kPartModel decoding"""

    def element_decode(self,
                       data: xs.ElementData,
                       xsd_element: 'XsdElement',
                       xsd_type=None,
                       level: int = 0):
        data_element = super().element_decode(data, xsd_element, xsd_type, level)
        obj = xsd_element.binding.xml_to_python(data_element)
        data_element.obj = obj
        return data_element


class MachineStitchTech:
    """ a MachineStitchTech for a given knitting machine model"""

    def __init__(self, **kwargs):
        """this is called only from binding class while decoding
        and when reading from the database
        use decode or from_dct as the  constuctor for this class"""
        for key, value in kwargs.items():
            setattr(self, key, value)
        if isinstance(self.kpPiecesList, list):
            self.kpPiecesList = {piece.pieceName: piece for piece in self.kpPiecesList}
        for _, piece in self.kpPiecesList.items():
            piece.configure_parts()


    @classmethod
    def decode(cls, xml):
        """the object is in res.obje ; res is the bindingclass of kpartmodel"""
        schema = helpers.create_schema(cls)
        converter = DMachineSTModelConverter()
        try:
            res = schema.decode(xml,
                                converter=converter
                                )
            if cls.__name__ == res.obj.__class__.__name__:
                try:
                    schema.validate(xml)
                    model = res.obj
                    return model
                except xs.XMLSchemaValidationError as e:
                    raise e
        except xs.XMLSchemaValidationError as e:
            raise e


if __name__ == '__main__':
    import unittest

    #from common.tools_for_log import logconf

    #mlog = logconf('__name__', test=True)

    from pathlib import Path
    current = Path(__file__)
    tests_dir = Path(current.parent, 'tests')
    loader = unittest.TestLoader()
    tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)
