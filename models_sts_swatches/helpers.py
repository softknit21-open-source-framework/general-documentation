#!/usr/bin/env python
# coding:utf-8
"""
 This file is part of SoftKnit21 framework

License
=======

 - B{SofKnit21} (U{http://www.softkni21.org}) is Copyright:
  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the Affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============
common tools for knitting models

Implements
==========


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL

"""

import cmath
from pathlib import Path
import importlib.util
import sys
import xmlschema as xs

#import SoftKnit21.generic_knitting_model.data.kPartModel_bindings as kpb

"""UNITS are the units which can be used in SVG description; this dictionnay gives the conversion factor 
to change measures to defaut unit which is mm"""
UNITS = {
    'mm': 1,
    'cm': 10,
    'in': 25.4,
    'pt': None,
    'pc': None,
    'px': None,
}

"""This Tolerance is used for all computations on pathPoints except for making horizontal pathpoints where the bigger tolerance 
named KMACHINE_ABSOLUTE_TOLERANCE is used;
"""
CMATH_ABS_TOLERANCE = 0.00001


"""knitting machine absolute tolerance depands on the type of knitting machine
this is the default absolute tolerance for a knitting machine ;
its evaluation is based on the swatch of a very thin yarn used in PASSAP magazine called  First experiences refers
where 1 row = 1.5mm ; this tolerance can be changed using the set_tolerance_function"""
KMACHINE_ABSOLUTE_TOLERANCE = 1.5


def set_tolerance(stringUnit):
    """set tolerance  for float calculation of coordinates :
    for a knit with manual machine gauge 1mm """
    global KMACHINE_ABSOLUTE_TOLERANCE
    tolerance = {'mm': 1, 'cm': 1e-1, 'in': 1e-3, 'pt': 1e-1, 'pc': 1, 'px': 1e-1}
    KMACHINE_ABSOLUTE_TOLERANCE = tolerance.get(stringUnit)


def stringforside(side):

    if side == 0:
        return 'left'
    else:
        return 'right'


def yEqual(point1, point2):
    """ returns True if self and other have almost equal y"""
    if isinstance(point2, complex):
        return cmath.isclose(point1.imag, point2.imag, abs_tol=CMATH_ABS_TOLERANCE)
    elif isinstance(point2, float):
        return cmath.isclose(point1.imag, point2, abs_tol=CMATH_ABS_TOLERANCE)


def xEqual(point1, point2):
    """ returns True if the self and other have almost equal x"""
    return cmath.isclose(point1.real, point2.real, abs_tol=CMATH_ABS_TOLERANCE)


def visualize_path(spath, filepath, addpoints=None, pathnodes=True, colors=None):
    """converts into svg an svgpathtools spath, and stores it into a file"""

    from svgpathtools.path import Arc, CubicBezier, QuadraticBezier
    from svgpathtools.paths2svg import wsvg
    # it seems that xmldom cannot use PosixPath
    filename = str(filepath)
    nodes = []
    node_colors = []
    if not colors and pathnodes and not addpoints:
        # different colors for each categorie of pathnodes
        startends = [segment.end for segment in spath[0:] if not isinstance(segment, Arc)]
        startends.append(spath[0].start)
        arcs = [segment.end for segment in spath[0:-1] if isinstance(segment, Arc)]
        control1s = [segment.control1 for segment in spath[0:] if isinstance(segment, CubicBezier)]
        control2s = [segment.control2 for segment in spath[0:] if isinstance(segment, CubicBezier)]
        controls = [segment.control for segment in spath[0:] if isinstance(segment, QuadraticBezier)]
        controls = control1s + control2s + controls
        nodes = startends + arcs + controls
        node_colors = ['red'] * len(startends) + ['blue'] * len(arcs) + ['green'] * len(controls)
    if not colors and addpoints and pathnodes:
        # all pathnodes are in black and addpoints are in color
        if isinstance(addpoints, complex):
            addpoints = [addpoints]
        for point in addpoints:
            nodes.append(point)
        node_colors += ['black'] * len(spath)
        mycolors = ['red', 'blue', 'green', 'black']
        c = 4
        p = len(addpoints)
        if c < p:
            mycolors = mycolors * (p % c)
        node_colors += mycolors[0: p]

    elif not colors and addpoints and not pathnodes:
         # draw curves in black with a few nodes in different colors
        mycolors = ['red', 'blue', 'green', 'black']
        c = 4
        p = len(addpoints)
        if c < p:
            mycolors = mycolors * (p % c)
        node_colors = mycolors[0: p]
        nodes = addpoints

    elif colors and not pathnodes:
        # draw paths in different colors
        c = len(colors)
        p = len(spath)
        if c < p:
            colors = colors * (p % c)
        colors = colors[0:p]
    elif colors and addpoints:
        nodes = addpoints
        node_colors = ['blue'] * len(addpoints)

    wsvg(spath,
         filename=filename,
         nodes=nodes,
         node_colors=node_colors,
         colors=colors,
         )


def get_schema_path(cls):
    """ returns the schema path corresponding to a class
    the schemas are in the data directory of the parent of this file containing helpers"""

    path = Path(__file__)
    filename = class_lower(cls) + '.xsd'
    return Path(path.parent, 'data', filename)


def class_lower(cls):
    if isinstance(cls, str):
        return cls[0].lower() + cls[1:]
    else:
        return cls.__name__[0].lower() + cls.__name__[1:]


def create_schema(model):
    """create the schema corresponding to the model, import bindings module and create the bindingcls.attribute for each element"""
    xsd_path = get_schema_path(model)
    binding_module_name = class_lower(model) + '_bindings'
    file_path = Path(xsd_path.parent, binding_module_name + '.py')
    spec = importlib.util.spec_from_file_location(binding_module_name, file_path)
    module = importlib.util.module_from_spec(spec)
    kpb = sys.modules[binding_module_name] = module
    spec.loader.exec_module(module)

    schema = xs.XMLSchema11(xsd_path,
                            base_url=xsd_path.parent,
                            loglevel=10)
    #not this iteration is different from iterating over schema.elements.values() or schema.elements;
    #this later one iterates only over global elements
    #do not use xs.schema createbindings because it would replace existing and create binding if get_binding is not linked to a binding class
    for xsd_component in schema.iter_components():

        if isinstance(xsd_component, xs.XsdElement):
            name = xsd_component.local_name
            bindingclassname = '{}Binding'.format(name.title().replace('_', ''))
            try:
                bindingcls = vars(kpb)[bindingclassname]
            except KeyError as e:
                raise e
            xsd_component.binding = bindingcls
    return schema






