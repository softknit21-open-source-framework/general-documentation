//=====Get Pattern File functions=====
int GetWord( QByteArray *Blob, int *i)
{//Return a little endian word
  int Tmp = ( Blob->at( *i) & 0xff) + (( Blob->at( *i + 1) & 0xff) * 256);
  *i += 2;
  return Tmp;
}
//------------------------------------
unsigned char GetByte( QByteArray *Blob, int *i)
{
  unsigned char Tmp = Blob->at( *i) & 0xff;
/*
  char TmpChr[ 10];
  sprintf( TmpChr, "0x%02x ", Tmp);
  qDebug() << *i << TmpChr;
*/
  *i += 1;
  return Tmp;
}
//------------------------------------
void inline SetPixel( QImage *StitchImage, int X, int Y, unsigned char Colour)
{
  QRgb Pixel;
  if ( Colour < 0x80)
    Pixel = qRgb( 180, 0, 180); // blue;
  else
    Pixel = qRgb( 255, 255, 255); // white;
  StitchImage->setPixel( X, Y, Pixel);
}
//------------------------------------
QImage MainWindow::ReadCutFile( QString Filename)
{
  QFile File( Filename);
  if (!File.open( QIODevice::ReadOnly))
  {
    QString Msg =  Filename + " not found\n";
    WriteTextToInformationTextEdit( Msg);
    return QImage();
  }
  QByteArray Blob = File.readAll();
  QString Msg = "Read " + Filename + " OK";
  WriteTextToInformationTextEdit( Msg);
//Decode .cut file

//Read header words
  int i = 0;
  int Size = Blob.size();
  int Width = GetWord( &Blob, &i);
  int Height = GetWord( &Blob, &i);
  int  Resvd = GetWord( &Blob, &i);
  if ( Resvd != 0)
  {
    Msg = "Reserved byte not zero i =" + QString::number( Resvd) +"\n";
    WriteTextToInformationTextEdit( Msg);
    return QImage();
  }
  i = 6;
  QImage *StitchImage = new QImage( Width, Height, QImage::Format_RGB32);
  unsigned char Colour = 0;

//Read lines with row info
  int RowCount = 0;
  int EncodedBytesCount = 0;
  bool LineError = false;
  while ( i < Size)
//  while ( i < 12)
  {
    int StitchCount = 0;
    int LineLength = GetWord( &Blob, &i);
    QByteArray TmpRow;
    for ( int j = 0; j < LineLength - 1; j++)
    {
      EncodedBytesCount = GetByte( &Blob, &i);
      int ByteCount = EncodedBytesCount & 0x7f;
      if (( EncodedBytesCount & 0x80) == 0)
      {//Read the following ByteCount bytes
        for ( int k = 0; k < ByteCount; k++)
        {
          Colour = GetByte( &Blob, &i);
          SetPixel( StitchImage, StitchCount, RowCount, Colour);
          if ( Colour > 0x80) TmpRow.append( static_cast <char>( 0x00)); else TmpRow.append( static_cast <char>( 0x01));
          StitchCount++;
          j++;
        }
      }
      else
      {//Repeat next byte by ByteCount times
        Colour = GetByte( &Blob, &i);
        j++;
        for ( int k = 0; k < ByteCount; k++)
        {
          SetPixel( StitchImage, StitchCount + k, RowCount, Colour);
          if ( Colour > 0x80) TmpRow.append( static_cast <char>( 0x00)); else TmpRow.append( static_cast <char>( 0x01));
        }
        StitchCount += ByteCount;
      }
    }
    StitchPatterns << TmpRow;
    TmpRow.clear();
    if ( GetByte( &Blob, &i) != 0) LineError = true;
    RowCount++;
  }
  if ( LineError)
  {
    Msg = "There was a line count error in reading " + Filename;
    WriteTextToInformationTextEdit( Msg);
    return QImage();
  }
  if ( RowCount != Height)
  {
    Msg = "\nThe number of rows (" + QString::number( RowCount) + ") does not equal the image height (" +
            QString::number( Height) + ") in " + Filename.mid( Filename.lastIndexOf( "/") + 1) + ". Can't proceed";
    WriteTextToInformationTextEdit( Msg);
  }
  return *StitchImage;
}
