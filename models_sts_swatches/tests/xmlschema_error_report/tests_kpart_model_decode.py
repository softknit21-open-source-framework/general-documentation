#!/usr/bin/env python
# coding:utf-8
"""  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

test for kp_knitting_model module

Implements
==========


Documentation
=============


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""

from pathlib import Path
import unittest
import importlib
import sys

import xmlschema as xs

from .kpart_model import KPartModel

#from commons.tests.testing_tools import Test_super_class

def create_schema(xsd_path):
    """create the schema corresponding to the model, import bindings module and create the bindingcls.attribute for each element"""
    binding_module_name = 'kPartModel' + '_bindings'
    file_path = Path(xsd_path.parent, binding_module_name + '.py')
    spec = importlib.util.spec_from_file_location(binding_module_name, file_path)
    module = importlib.util.module_from_spec(spec)
    kpb = sys.modules[binding_module_name] = module
    spec.loader.exec_module(module)

    schema = xs.XMLSchema11(xsd_path,
                            base_url=xsd_path.parent,
                            loglevel=10)
    #not this iteration is different from iterating over schema.elements.values() or schema.elements;
    #this later one iterates only over global elements
    #do not use xs.schema createbindings because it would replace existing and create binding if get_binding is not linked to a binding class
    for xsd_component in schema.iter_components():

        if isinstance(xsd_component, xs.XsdElement):
            name = xsd_component.local_name
            bindingclassname = '{}Binding'.format(name.title().replace('_', ''))
            try:
                bindingcls = vars(kpb)[bindingclassname]
            except KeyError as e:
                raise e
            xsd_component.binding = bindingcls
    return schema



class Test_kpart_model_errors(unittest.TestCase):
    """ test assertions and restriction raise error message"""

    @ classmethod
    def setUpClass(cls):
        xsd_path = Path(Path(__file__).parent, 'kPartModel.xsd')
        cls.schema = create_schema(xsd_path)
        #cls.converter = KpartModelConverter(namespaces=[cls.schema.default_namespace])

    def setUp(self):
        test_id = self.id().split('.')[-1]
        self.xml_errors_dir = Path(Path(__file__).parent, 'errors')
        self.file = next((path.stem for path in self.xml_errors_dir.iterdir() if path.is_file() and test_id[7:] in path.stem))


    def test01_duplicate_pieceName(self):
        """test unique-pieceName in kPartModel of  kPartModel.xsd """
        #this test does not detect any error in the assertion

        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()

        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            KPartModel.decode(xml)

        ex = context.exception
        self.assertTrue("duplicated value" in ex.reason)
        self.assertTrue("Xsd11Unique" in ex.reason)
        self.assertTrue("unique-pieceName" in ex.reason)
        self.assertTrue('kPartsPiece' in ex.obj.tag)

    def test02_duplicate_partNumber(self):
        """unique-part-number in kpPiece of kPartModel.xsd"""
        #this test passes

        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()

        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            KPartModel.decode(xml)

        ex = context.exception
        self.assertTrue("duplicated value (0," in ex.reason)
        self.assertTrue("Xsd11Unique" in ex.reason)
        self.assertTrue('unique-part-number' in ex.reason)

    def test03_invalid_nexts(self):
        """test test-nexts assertion in kpPiece of kPartModel.xsd"""
        #this test does not detect the false assertion thus fails when contstructing the object.
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()

        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            KPartModel.decode(xml)

        ex = context.exception
        self.assertEqual(ex.validator.id, 'test-nexts')

    def test04_invalid_previous(self):
        """ test test-previous assertion in kpPiece of kPartModel.xsd"""
        #this test does not detect the false assertion and creates only one kPart
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()

        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            KPartModel.decode(xml)

        ex = context.exception
        self.assertEqual(ex.validator.id, 'test-previous')

    def test05_join_less_than2previous(self):
        """test kpRefsListmin2 in joinPart of kPartModel.xsd"""
        #this test passes
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()
        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            KPartModel.decode(xml)

        ex = context.exception
        self.assertTrue('value length' in ex.reason)


    def test06_base_notHorizontal(self):
        """ test test-base-horizontal assertion in baseSeg of kPartModel.xsd"""
        #this test does not pass
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()
        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            KPartModel.decode(xml)

        ex = context.exception
        self.assertEqual(ex.validator.id, "test-base-horizontal")
        
    def test07_base_Horizontal(self):
        """ this test raises an error while the base is horizontal"""
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()

            KPartModel.decode(xml)

    def test08_horizontal_line_notHorizontal(self):
        """ test horizontal-line assertion in complexType horizontal-line of kPartModel.xsd"""
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()
        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            KPartModel.decode(xml)

        ex = context.exception
        self.assertEqual(ex.validator.id, "test-horizontal-line")

    def test09_vertical_line_notVertical(self):
        #this test passes
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()
        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            KPartModel.decode(xml)

        ex = context.exception
        self.assertEqual(ex.validator.id, "test-vertical-line")
        
    def test10_vertical_line_Vertical(self):
        #this test fails while the line is vertical
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()
            KPartModel.decode(xml)

       


    #def test09_lastnotCastOffPart(self):
        #with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            #xml = f.read()
        #with self.assertRaises(xs.XMLSchemaValidationError) as context:
            #kpm.KPartModel.decode(xml)

        #ex = context.exception
        #self.assertEqual(ex.validator.id, "test-end-castOff")

    #def test11_hole_in_partNumbers(self):
        #"""test missing partNumber in kPartsList of kPartModel.xsd"""
        #with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            #xml = f.read()
        #with self.assertRaises(xs.XMLSchemaValidationError) as context:
            #KPartModel.decode(xml)

        #ex = context.exception
        #self.assertEqual(ex.validator.id, "test-missing-part-number")


if __name__ == '__main__':
    import unittest

    unittest.main()
