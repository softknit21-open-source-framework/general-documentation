#!/usr/bin/env python
# coding:utf-8
"""  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

test for kp_knitting_model module

Implements
==========

 
Documentation
=============
 

Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""

from pathlib import Path
import unittest
#from svgpathtools.path import Line, Arc, CubicBezier, QuadraticBezier, Path as sPath

import svgpathtools as svp


from commons.tests.testing_tools import Test_super_class
import models_sts_swatches.kpart_model as kpm
from models_sts_swatches.helpers import visualize_path


class Test_KPart_functions (Test_super_class, unittest.TestCase):

    @ classmethod
    def setUpClass(cls):
        path = super().get_tests_datadir(cls, __file__)
        xml_path = Path(path.parent, 'xml', 'manysingulars.xml')
        with open(xml_path, 'r') as f:
            cls.xml_many = f.read()

    def setUp(self):
        many_model = kpm.KPartModel.decode(self.xml_many)
        self.mkPartsList = many_model.kpPiecesList['pieceknit_layer1'].kPartsList

    def test1__lt__(self):
        s = sorted([self.mkPartsList[5], self.mkPartsList[1], self.mkPartsList[4]])

        self.assertEqual(s, [self.mkPartsList[1], self.mkPartsList[4], self.mkPartsList[5]])

        s = sorted([self.mkPartsList[8], self.mkPartsList[9], self.mkPartsList[10]])

        self.assertEqual(s, [self.mkPartsList[8], self.mkPartsList[10], self.mkPartsList[9]])

        #with castOnPoint
        s = sorted([self.mkPartsList[7], self.mkPartsList[3]])

        self.assertEqual(s, [self.mkPartsList[3], self.mkPartsList[7]])

    def test2_get_endpoints_seg_part(self):
        p = self.mkPartsList[10]

        left, right = p.endPoints

        self.assertEqual(left, p.base.start)
        self.assertEqual(right, p.base.end)

        p = self.mkPartsList[12]

        left, right = p.endPoints

        self.assertEqual(left, p.base.start)
        self.assertEqual(right, p.base.end)

    def test3_get_endpoints_standard_part(self):
        p = self.mkPartsList[1]

        left, right = p.endPoints

        self.assertEqual(left, p.bordersList[0][-1].seg.end)
        self.assertEqual(right, p.bordersList[1][-1].seg.end)

    def test4_to_path_Caston_segment(self):
        p = self.mkPartsList[0]
        sp = p.to_path()

        self.assertTrue(isinstance(sp, svp.Path))
        self.assertEqual(sp._segments[0].start, p.bordersList[0][-1].seg.end)
        self.assertEqual(sp._segments[-1].end, p.bordersList[1][-1].seg.end)
        self.assertTrue(sp.iscontinuous())
        self.assertTrue(len(sp), 3)

    def test5_to_path_Caston_point(self):
        p = self.mkPartsList[2]
        sp = p.to_path()

        self.assertTrue(isinstance(sp, svp.Path))
        self.assertEqual(sp._segments[0].start, p.bordersList[0][-1].seg.end)
        self.assertEqual(sp._segments[-1].end, p.bordersList[1][-1].seg.end)
        self.assertTrue(sp.iscontinuous())
        self.assertTrue(len(sp), 2)

    def test6_to_path_other_parts(self):
        # join
        p = self.mkPartsList[6]

        with self.assertRaises(NotImplementedError):
            sp = p.to_path()

        # splits
        p = self.mkPartsList[8]

        with self.assertRaises(NotImplementedError):
            sp = p.to_path()

        # castoff
        p = self.mkPartsList[12]

        with self.assertRaises(NotImplementedError):
            sp = p.to_path()

    # @unittest.skip
    def test7_add_to_path_generic(self):
        """change end of part0 to make it contiguous with part 1"""
        p = self.mkPartsList[0]
        p1 = self.mkPartsList[1]
        p2 = self.mkPartsList[2]
        p.bordersList[1][-1].seg.end = p2.bordersList[1][-1].seg.end
        sp = p.to_path()

        newp, stack = p1.add_to_path(sp)

        self.assertTrue(isinstance(newp, svp.Path))
        self.assertEqual(newp.start, p1.endPoints[0])
        self.assertEqual(newp.end, p1.endPoints[1])
        self.assertTrue(newp.iscontinuous())
        self.assertEqual(len(newp), 7)

        self.assertEqual(stack, [])

    def test8_is_contiguous(self):
        pass


#@unittest.skip('temporaire')
class Test_KPartsIterable_functions_with_mock (Test_super_class, unittest.TestCase):

    def setUp(self):
        """create mockKParts with just previous, nexts and partNumber attribute"""

        class MK:
            def __init__(self, number, kwargs={}):
                self.previous = kwargs.get('previous', [])
                self.nexts = kwargs.get('nexts', [])
                self.partNumber = number

            def __repr__(self):
                return f'pk{self.partNumber}'

            def __lt__(self, other):
                return True

        self.parts = []
        for i in range(0, 13):
            a = MK(i)
            setattr(self, 'pk' + str(i), a)
            self.parts.append(a)
        self.pk1.previous = [self.pk0, self.pk2]
        self.pk3.previous = [self.pk1, self.pk5, self.pk4]
        self.pk6.previous = [self.pk3, self.pk7]
        self.pk8.previous = [self.pk6]
        self.pk9.previous = [self.pk6]
        self.pk10.previous = [self.pk6]
        self.pk11.previous = [self.pk9]
        self.pk12.previous = [self.pk8]

        self.pk0.nexts = [self.pk1]
        self.pk1.nexts = [self.pk3]
        self.pk2.nexts = [self.pk1]
        self.pk3.nexts = [self.pk6]
        self.pk4.nexts = [self.pk3]
        self.pk5.nexts = [self.pk3]
        self.pk6.nexts = [self.pk8, self.pk9, self.pk10]
        self.pk7.nexts = [self.pk6]
        self.pk8.nexts = [self.pk12]
        self.pk9.nexts = [self.pk11]

    def test_iter(self):
        kpartsit = kpm.KPartsIterable(self.parts)
        res = []

        for kp in kpartsit:
            res.append(kp)

        self.assertEqual(res, [self.pk0, self.pk2, self.pk1, self.pk4, self.pk5, self.pk3, self.pk7,
                               self.pk6, self.pk10, self.pk9, self.pk11, self.pk8, self.pk12, ])

    def test_gen_all_previous(self):
        """ test all previous result for part6"""
        kpartsit = kpm.KPartsIterable(self.parts)
        kpartsit.stack = []
        kpartsit.done = []
        kpartsit.current = self.pk6
        gen = kpartsit.gen_all_previous(self.pk6)
        rs = []

        for kp in gen:
            rs.append(kp.partNumber)
        self.assertEqual(rs, [3, 1, 0, 2, 5, 4, 7])


#@unittest.skip
class Test_KPartsIterable_total_manysingulars (Test_super_class, unittest.TestCase):

    @ classmethod
    def setUpClass(cls):
        path = super().get_tests_datadir(cls, __file__)
        xml_path = Path(path.parent, 'xml', 'manysingulars.xml')
        with open(xml_path, 'r') as f:
            xml = f.read()
        kpmodel = kpm.KPartModel.decode(xml)
        cls.kPartsList = kpmodel.kpPiecesList['pieceknit_layer1'].kPartsList
        #super().print_kparts_iterable(cls, __file__, cls.kPartsList, 'manysingulars')

    def test_manysingulars(self):
        kpartsit = kpm.KPartsIterable(self.kPartsList)
        res = []

        for kp in kpartsit:
            res.append(kp.partNumber)

        self.assertEqual(res, [0, 2, 1, 4, 5, 3, 7, 6, 10, 8,
                               12, 9, 11])

    def test_manysingulars_no_splitseg_priority(self):
        kpartsit = kpm.KPartsIterable(self.kPartsList,
                                      split_segment_first=False)
        res = []

        for kp in kpartsit:
            res.append(kp.partNumber)

        self.assertEqual(res, [0, 2, 1, 4, 5, 3, 7, 6, 8,
                               12, 10, 9, 11])

    def test_manysingulars_rightpriority(self):
        kpartsit = kpm.KPartsIterable(self.kPartsList,
                                      priority='right')
        res = []

        for kp in kpartsit:
            res.append(kp.partNumber)

        self.assertEqual(res, [0, 2, 1, 5, 4, 3, 7, 6, 10,
                               9, 11, 8, 12])

    def test_manysingulars_first_7(self):
        kpartsit = kpm.KPartsIterable(self.kPartsList,
                                      priority='right',
                                      first_index=7)
        res = []

        for kp in kpartsit:
            res.append(kp.partNumber)

        self.assertEqual(res, [7, 2, 0, 1, 5, 4, 3, 6,
                               10, 9, 11, 8, 12])


#@unittest.skip('temporaire')
class Test_KPartsIterable_total_splits_and_joins (Test_super_class, unittest.TestCase):
    @ classmethod
    def setUpClass(cls):
        path = super().get_tests_datadir(cls, __file__)
        xml_path = Path(path.parent, 'xml', 'splits_and_joins.xml')
        with open(xml_path, 'r') as f:
            xml = f.read()
        model = kpm.KPartModel.decode(xml)
        cls.kPartsList = model.kpPiecesList['pieceknit_layer1'].kPartsList
        #super().print_kparts_iterable(cls, __file__, cls.kPartsList, 'splits_and_joins')

    def test_split_and_joins(self):
        kpartsit = kpm.KPartsIterable(self.kPartsList)
        res = []

        for kp in kpartsit:
            res.append(kp.partNumber)

        self.assertEqual(res, [0, 3, 2, 6, 4, 5, 1, 10, 7, 26, 8, 25, 9, 11, 24, 12,
                               14, 17, 13, 20, 23, 21, 22, 18, 15, 16, 19])

    def test_split_and_joins_first_14(self):
        kpartsit = kpm.KPartsIterable(self.kPartsList,
                                      first_index=14)
        res = []

        for kp in kpartsit:
            res.append(kp.partNumber)

        self.assertEqual(res, [14, 17, 18, 3, 2, 6, 0, 4, 5, 1, 10, 7, 26, 8,
                               25, 9, 11, 24, 12, 13, 20, 23, 21, 22, 15, 16, 19])


class Test_draw_piece_manysingulars (Test_super_class, unittest.TestCase):
    """ test draw piece need to check result visually"""

    @ classmethod
    def setUpClass(cls):
        cls.path = super().get_tests_datadir(cls, __file__)
        xml_path = Path(cls.path.parent, 'xml', 'manysingulars.xml')
        with open(xml_path, 'r') as f:
            xml = f.read()
        model = kpm.KPartModel.decode(xml)
        cls.kpiece = model.kpPiecesList['pieceknit_layer1']

    def tests_draw_piece(self):
        to_display = self.kpiece.draw()
        file = Path(self.path, 'created_svg', 'manysingulars.svg')
        addpoints = []
        #add points on the cut of CubicBezier
        for segment in to_display._segments:
            if isinstance(segment, svp.CubicBezier) or isinstance(segment, svp.QuadraticBezier):
                addpoints.append(segment.start)
        visualize_path(to_display, file, pathnodes=False, addpoints=addpoints)


class Test_draw_piece_splits_and_joins (Test_super_class, unittest.TestCase):
    """ to check if this test passes need to visually compare created svg with original svg """

    @ classmethod
    def setUpClass(cls):
        cls.path = super().get_tests_datadir(cls, __file__)
        xml_path = Path(cls.path.parent, 'xml', 'splits_and_joins.xml')
        with open(xml_path, 'r') as f:
            xml = f.read()
        model = kpm.KPartModel.decode(xml)
        cls.kpiece = model.kpPiecesList['pieceknit_layer1']

    def tests_draw_piece(self):
        to_display = self.kpiece.draw()
        file = Path(*self.path.parts, 'created_svg', 'split_and_joins.svg')
        addpoints = []
        for segment in to_display._segments:
            if isinstance(segment, svp.CubicBezier) or isinstance(segment, svp.QuadraticBezier):
                addpoints.append(segment.start)
        visualize_path(to_display, file, pathnodes=False, addpoints=addpoints)


if __name__ == '__main__':
    import unittest

    #from common.tools_for_log import logconf

    #mlog = logconf('__name__', test=True)

    unittest.main()
