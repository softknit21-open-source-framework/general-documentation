# -*- coding: utf-8 -*-
""" This file is part of SoftKnit21 framework

License
=======

 - B{SofKnit21} (U{http://www.softkni21.org}) is Copyright:
  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

gridPattern Tests

Implements
==========

 - B{test_grid_pattern}:


Documentation
=============



Usage
=====

@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

from pathlib import Path
import copy

from models_sts_swatches.grid_pattern import GridPattern
from models_sts_swatches.grid_pattern_cut_decoder import decodeGridPatternCut
from commons.tests.testing_tools import Test_super_class


LINE_LIST_1008 = ['00010100', '00100010', '01000001', '10000000', '01000001', '00100010', '00010100', '00001000']
LINE_LIST_1123 = ['0100010001',
                  '1000111000',
                  '0001111100',
                  '0011101110',
                  '0111000111',
                  '0110000011',
                  '0100000001',
                  '0010000010',
                  '0001000100',
                  '1000101000',
                  '1100010001',
                  '1110000011',
                  '0111000111',
                  '0011101110',
                  '0001101100',
                  '0000101000',
                  '0001000100',
                  '0010000010'
                  ]


class Test_GridPattern_decoder_cut (Test_super_class):
    """ tests for the check_constraints method"""
    
    @classmethod
    def setUpClass(cls):
        datadir = super().get_tests_datadir(cls, __file__)
        cls.file1008 = Path(datadir, 'data_1008.CUT')
        cls.file1123 = Path(datadir, '1123 Inverted.CUT')
        
    def test_decoder_PASSAP(self):
        with open(self.file1008, 'rb') as f:
            width, height, line_list = decodeGridPatternCut(f)

        self.assertEqual(width, 8)
        self.assertEqual(height, 8)
        self.assertEqual(line_list, LINE_LIST_1008)

    def test_decoder_color_code(self):
        with open(self.file1123, 'rb') as f:
            width, height, line_list = decodeGridPatternCut(f, typ='colorNumber')
        self.assertEqual(width, 10)
        self.assertEqual(height, 18)
        self.assertEqual(line_list, LINE_LIST_1123[::-1])
        


